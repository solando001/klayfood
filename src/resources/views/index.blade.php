@extends('layouts.master')
@section('title', 'KlayFood - HomePage')
@section('content')

    <!-- Start Main Banner Area -->
    <div class="main-banner">
        <div class="main-banner-item">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content">
                            <span>Take Natural Taste From Our Shop</span>
                            <h1>Organic Food Is Good For Health</h1>
{{--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua quis ipsum suspendisse</p>--}}
                            <div class="banner-btn">
{{--                                <a href="#" class="default-btn">Our Shop</a>--}}
{{--                                <a href="#" class="optional-btn">Add to Cart</a>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Banner Area -->



    <!-- Start Top Products Area -->
    <section class="top-products-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <h2>Trending Products</h2>
{{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua quis ipsum suspendisse</p>--}}
            </div>


            <div class="featured-products-slider owl-carousel owl-theme">
                @foreach($products as $product)
                    <div class="featured-products-item">
                        <div class="products-image">
                            <a href="{{url('product/'.$product->slug)}}"><img src="{{$product->image->url}}" alt="image"></a>

                            <ul class="products-action">
                                <li>
                                    <a href="{{url('add-to-cart/'.$product->slug)}}" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>
                                </li>
                                {{--                            <li>--}}
                                {{--                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
                                {{--                            </li>--}}
                                <li>
                                    <a href="{{url('product/'.$product->slug)}}" data-tooltip="tooltip" data-placement="top" title="Product View">
                                        <i class="flaticon-search"></i>
                                    </a>
{{--                                <li>--}}
{{--                                    <a href="#" data-tooltip="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#productsQuickViews_{{$product->id}}">--}}
{{--                                        <i class="flaticon-search"></i>--}}
{{--                                    </a>--}}
                                </li>
                            </ul>

                            <div class="new">
                                <span>New</span>
                            </div>
                        </div>

                        <div class="products-content">
                            <h3>
                                <a href="{{url('product/'.$product->slug)}}">{{$product->name}}</a>
                            </h3>
                            <div class="price">
                                <span class="new-price">{{number_format($product->price,2)}}</span>
                                {{--                            <span class="old-price">$12.00</span>--}}
                            </div>
                            {{--                        <ul class="rating">--}}
                            {{--                            <li>--}}
                            {{--                                <i class='bx bxs-star'></i>--}}
                            {{--                                <i class='bx bxs-star'></i>--}}
                            {{--                                <i class='bx bxs-star'></i>--}}
                            {{--                                <i class='bx bxs-star'></i>--}}
                            {{--                                <i class='bx bx-star'></i>--}}
                            {{--                            </li>--}}
                            {{--                        </ul>--}}
                        </div>
                    </div>





                    <!-- Start QuickView Modal Area -->
{{--                    <div class="modal fade productsQuickViews" id="productsQuickViews_{{$product->id}}" tabindex="-1" role="dialog" aria-hidden="true">--}}
{{--                        <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--                            <div class="modal-content">--}}
{{--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                    <span aria-hidden="true"><i class="flaticon-cancel"></i></span>--}}
{{--                                </button>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <div class="products-image"><img src="{{asset($product->image->url)}}"></div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <div class="product-content">--}}
{{--                                            <h3>{{$product->name}}</h3>--}}
{{--                                            <div class="price">--}}
{{--                                                <span class="new-price">&#8358;{{number_format($product->price)}}</span>--}}
{{--                                                --}}{{--                                                <span class="old-price">$652.00</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="product-review">--}}
{{--                                                --}}{{--                                                <div class="rating">--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                </div>--}}
{{--                                                --}}{{--                                                <a href="#" class="rating-count">5 reviews</a>--}}
{{--                                            </div>--}}
{{--                                            <p>{!! $product->description !!}</p>--}}
{{--                                            <div class="product-add-to-cart">--}}
{{--                                                <div class="input-counter">--}}
{{--                                        <span class="minus-btn">--}}
{{--                                            <i class='bx bx-minus'></i>--}}
{{--                                        </span>--}}
{{--                                                    <input type="text" value="1">--}}
{{--                                                    <span class="plus-btn">--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                        </span>--}}
{{--                                                </div>--}}
{{--                                                <a href="{{url('add-to-cart/'.$product->slug)}}" class="default-btn">--}}
{{--                                                    Add to cart--}}
{{--                                                </a>--}}
{{--                                            </div>--}}

{{--                                            --}}{{--                                            <div class="buy-checkbox-btn">--}}
{{--                                            --}}{{--                                                <div class="item">--}}
{{--                                            --}}{{--                                                    <input class="inp-cbx" id="cbx" type="checkbox">--}}
{{--                                            --}}{{--                                                    <label class="cbx" for="cbx">--}}
{{--                                            --}}{{--                                            <span>--}}
{{--                                            --}}{{--                                                <svg width="12px" height="10px" viewbox="0 0 12 10">--}}
{{--                                            --}}{{--                                                    <polyline points="1.5 6 4.5 9 10.5 1"></polyline>--}}
{{--                                            --}}{{--                                                </svg>--}}
{{--                                            --}}{{--                                            </span>--}}
{{--                                            --}}{{--                                                        <span>I agree with the terms and conditions</span>--}}
{{--                                            --}}{{--                                                    </label>--}}
{{--                                            --}}{{--                                                </div>--}}
{{--                                            --}}{{--                                                <div class="item">--}}
{{--                                            --}}{{--                                                    <a href="#" class="btn btn-light">Buy it now!</a>--}}
{{--                                            --}}{{--                                                </div>--}}
{{--                                            --}}{{--                                            </div>--}}

{{--                                            --}}{{--                                            <div class="products-share">--}}
{{--                                            --}}{{--                                                <ul class="social">--}}
{{--                                            --}}{{--                                                    <li><span>Share:</span></li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                </ul>--}}
{{--                                            --}}{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <!-- End QuickView Modal Area -->
                @endforeach
            </div>




            {{--            <div class="row">--}}
{{--                @foreach($products as $product)--}}
{{--                    <div class="col-lg-3 col-md-6">--}}
{{--                        <div class="top-products-item">--}}
{{--                            <div class="products-image">--}}
{{--                                <a href="{{url('product/'.$product->slug)}}"><img src="{{$product->image->url}}" alt="image"></a>--}}

{{--                                <ul class="products-action">--}}
{{--                                    <li>--}}
{{--                                        <a href="{{url('add-to-cart/'.$product->slug)}}" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a href="#" data-tooltip="tooltip" data-placement="top" title="Product Quick View" data-toggle="modal" data-target="#productsQuickView_{{$product->id}}">--}}
{{--                                            <i class="flaticon-search"></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}

{{--                                <div class="sale">--}}
{{--                                    <span>Sale</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="products-content">--}}
{{--                                <h3>--}}
{{--                                    <a href="{{url('product/'.$product->slug)}}">{{$product->name}}</a>--}}
{{--                                </h3>--}}
{{--                                <div class="price">--}}
{{--                                    <span class="new-price">&#8358;{{number_format($product->price,2)}}</span>--}}
{{--                                    --}}{{--                                <span class="old-price">$125.00</span>--}}
{{--                                </div>--}}
{{--                                --}}{{--                            <ul class="rating">--}}
{{--                                --}}{{--                                <li>--}}
{{--                                --}}{{--                                    <i class='bx bxs-star'></i>--}}
{{--                                --}}{{--                                    <i class='bx bxs-star'></i>--}}
{{--                                --}}{{--                                    <i class='bx bxs-star'></i>--}}
{{--                                --}}{{--                                    <i class='bx bxs-star'></i>--}}
{{--                                --}}{{--                                    <i class='bx bx-star'></i>--}}
{{--                                --}}{{--                                </li>--}}
{{--                                --}}{{--                            </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}



{{--                    <!-- Start QuickView Modal Area -->--}}
{{--                    <div class="modal fade productsQuickView" id="productsQuickView_{{$product->id}}" tabindex="-1" role="dialog" aria-hidden="true">--}}
{{--                        <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--                            <div class="modal-content">--}}
{{--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                    <span aria-hidden="true"><i class="flaticon-cancel"></i></span>--}}
{{--                                </button>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <div class="products-image"><img src="{{asset($product->image->url)}}"></div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <div class="product-content">--}}
{{--                                            <h3>{{$product->name}}</h3>--}}
{{--                                            <div class="price">--}}
{{--                                                <span class="new-price">&#8358;{{number_format($product->price)}}</span>--}}
{{--                                                --}}{{--                                                <span class="old-price">$652.00</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="product-review">--}}
{{--                                                --}}{{--                                                <div class="rating">--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                </div>--}}
{{--                                                --}}{{--                                                <a href="#" class="rating-count">5 reviews</a>--}}
{{--                                            </div>--}}
{{--                                            <p>{!! $product->description !!}</p>--}}
{{--                                            <div class="product-add-to-cart">--}}
{{--                                                <div class="input-counter">--}}
{{--                                        <span class="minus-btn">--}}
{{--                                            <i class='bx bx-minus'></i>--}}
{{--                                        </span>--}}
{{--                                                    <input type="text" value="1">--}}
{{--                                                    <span class="plus-btn">--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                        </span>--}}
{{--                                                </div>--}}
{{--                                                <a href="{{url('add-to-cart/'.$product->slug)}}" class="default-btn">--}}
{{--                                                    Add to cart--}}
{{--                                                </a>--}}
{{--                                            </div>--}}

{{--                                            <div class="buy-checkbox-btn">--}}
{{--                                                <div class="item">--}}
{{--                                                    <input class="inp-cbx" id="cbx" type="checkbox">--}}
{{--                                                    <label class="cbx" for="cbx">--}}
{{--                                            <span>--}}
{{--                                                <svg width="12px" height="10px" viewbox="0 0 12 10">--}}
{{--                                                    <polyline points="1.5 6 4.5 9 10.5 1"></polyline>--}}
{{--                                                </svg>--}}
{{--                                            </span>--}}
{{--                                                        <span>I agree with the terms and conditions</span>--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                                <div class="item">--}}
{{--                                                    <a href="#" class="btn btn-light">Buy it now!</a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            --}}{{--                                            <div class="products-share">--}}
{{--                                            --}}{{--                                                <ul class="social">--}}
{{--                                            --}}{{--                                                    <li><span>Share:</span></li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                </ul>--}}
{{--                                            --}}{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- End QuickView Modal Area -->--}}
{{--                @endforeach--}}
{{--            </div>--}}
        </div>
    </section>
    <!-- End Top Products Area -->


    <!-- Start Why Choose Area -->
    <section class="why-choose-area pt-100">
        <div class="container">
            <div class="why-choose-item">
                <div class="content">
                    <span>Why Choose Us</span>
{{--                    <h3>We are Ensure of These Types</h3>--}}
                    <p>Klayfoods is an innovative food production and distribution company aimed at improving healthy eating lifestyle in Nigeria and africa in the most affordable way.</p>
                        <p>
                            We like to call our Klayfood Tombrown a combination of oats and golden morn, because it has a similar taste and texture of Golden Morn and it is prepared like oats
                        </p>
                </div>

                <div class="inner-content">
                    <div class="icon">
                        <i class="flaticon-leaf"></i>
                    </div>
                    <h4>Organic</h4>
                    <p>The product of klayfoods is a special Tom Brown Recipe. Tom Brown is a special blend of natural grains which includes
                        whole grains, Ground nut, Soya Beans and Dates. The product is purely organic and the results is a wholesome meal,
                        rich in nutrients, and suitable for both babies and adults.
                    </p>
                </div>

                <div class="inner-content">
                    <div class="icon">
                        <i class="flaticon-heart"></i>
                    </div>
                    <h4>Fast and Easy</h4>
                    <p>The Klay Foods TomBrown are quick and easy to prepare. It can be made in the comfort of your home and office.
                        It’s just a 5 minutes meal. This will save you time especially if you are on the go.
                    </p>
                </div>

                <div class="inner-content">
                    <div class="icon">
                        <i class="flaticon-plant"></i>
                    </div>
                    <h4>High Quality</h4>
                    <p>Tom Brown can be locally made by acquiring the ingredients and blending together. klayFods proprietary blend is made
                        from specially selected grains. Its recipe, unlike other locally made brands, was put together after years of
                        research into the nutritional value of each Grain component and the quantity necessary to produce a perfect
                        healthy blend of Tom Brown.
                    </p>
                </div>

                <div class="inner-content">
                    <div class="icon">
                        <i class="flaticon-nature"></i>
                    </div>
                    <h4>Affordable</h4>
                    <p>Klayfoods Tom Brown is currently packed in a 1kg which cost 1,800( one thousand eight hundred naira only)
                        and 500g pack which costs 1200 ( one thousand two hundred Naira Only)
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Why Choose Area -->





    <!-- Start Department Area -->
    <section class="department-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <h2>KlayFoods Organic Premium Categories</h2>
{{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua quis ipsum suspendisse</p>--}}
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="single-department">
                        <div class="department-image">
                            <a href="#">
                                <img src="assets/img/Oloyinmomo.jpg" alt="image">
                            </a>

                            <div class="content">
                                <h3>
                                    <a href="#" style="color: white">Oloyinmomo Package - &#8358;6,000</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="single-department">
                        <div class="department-image">
                            <a href="#">
                                <img src="assets/img/Afya_Package.jpg" alt="image">
                            </a>

                            <div class="content">
                                <h3>
                                    <a href="#" style="color: white">Afya Package - &#8358;5,500</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="single-department">
                        <div class="department-image">
                            <a href="#">
                                <img src="assets/img/Klayfood.jpg" alt="image">
                            </a>

                            <div class="content">
                                <h3>
                                    <a href="#" style="color: white">Klayfood Signature Wood Cereal Bowl Package - &#8358;4,500</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Department Area -->







    <!-- Start Detox Water Area -->
    <section class="detox-water-area ptb-100">
        <div class="container">
            <div class="section-title">
                <h2>Klayfoods</h2>
{{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua quis ipsum suspendisse</p>--}}
            </div>

            <div class="detox-water-image">
                <img src="assets/img/combo1.jpg" alt="image">
            </div>
        </div>

        <div class="detox-water-shape">
            <div class="shape1">
                <img src="assets/img/detox-water/detox-water-1.png" alt="image">
            </div>

            <div class="shape2">
                <img src="assets/img/detox-water/detox-water-2.png" alt="image">
            </div>

            <div class="shape3">
                <img src="assets/img/detox-water/detox-water-3.png" alt="image">
            </div>

            <div class="shape4">
                <img src="assets/img/detox-water/detox-water-4.png" alt="image">
            </div>
        </div>
    </section>
    <!-- End Detox Water Area -->


    <!-- Start Overview Area -->
    <section class="overview-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6">
                    <div class="overview-content">
                        <h3>20% Off In This Week</h3>
                        <p>Our cereal bowls and TomBrown packs are going off at a whole 20% OFF this week! Take advantage
                            of the sales and get yours at the lowest prices ever.</p>

{{--                        <div class="overview-btn">--}}
{{--                            <a href="shop-1.html" class="default-btn">Discover Now</a>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="overview-video">
                        <a href="https://www.youtube.com/watch?v=1AkajEMoPNk" class="video-btn popup-youtube">
                            <i class='bx bxl-youtube'></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Overview Area -->

    <!-- Start Most Viewed Products Area -->
    <section class="featured-products-area bg-fafafa pt-100 pb-100">
        <div class="container">
            <div class="section-title">
                <h2>Most Viewed Products This Month</h2>
{{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua quis ipsum suspendisse</p>--}}
            </div>

            <div class="featured-products-slider owl-carousel owl-theme">
                @foreach($products as $product)
                <div class="featured-products-item">
                    <div class="products-image">
                        <a href="{{url('product/'.$product->slug)}}"><img src="{{$product->image->url}}" alt="image"></a>

                        <ul class="products-action">
                            <li>
                                <a href="{{url('add-to-cart/'.$product->slug)}}" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>
                            </li>
{{--                            <li>--}}
{{--                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#productsQuickViews_{{$product->id}}">--}}
{{--                                    <i class="flaticon-search"></i>--}}
{{--                                </a> --}}
                                <li>
                                <a href="{{url('product/'.$product->slug)}}" data-tooltip="tooltip" data-placement="top" title="Product View">
                                    <i class="flaticon-search"></i>
                                </a>
                            </li>
                        </ul>

                        <div class="new">
                            <span>New</span>
                        </div>
                    </div>

                    <div class="products-content">
                        <h3>
                            <a href="{{url('product/'.$product->slug)}}">{{$product->name}}</a>
                        </h3>
                        <div class="price">
                            <span class="new-price">{{number_format($product->price,2)}}</span>
{{--                            <span class="old-price">$12.00</span>--}}
                        </div>
{{--                        <ul class="rating">--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-star'></i>--}}
{{--                                <i class='bx bxs-star'></i>--}}
{{--                                <i class='bx bxs-star'></i>--}}
{{--                                <i class='bx bxs-star'></i>--}}
{{--                                <i class='bx bx-star'></i>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
                    </div>
                </div>





                    <!-- Start QuickView Modal Area -->
{{--                    <div class="modal fade productsQuickViews" id="productsQuickViews_{{$product->id}}" tabindex="-1" role="dialog" aria-hidden="true">--}}
{{--                        <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--                            <div class="modal-content">--}}
{{--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                    <span aria-hidden="true"><i class="flaticon-cancel"></i></span>--}}
{{--                                </button>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <div class="products-image"><img src="{{asset($product->image->url)}}"></div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-lg-6 col-md-6">--}}
{{--                                        <div class="product-content">--}}
{{--                                            <h3>{{$product->name}}</h3>--}}
{{--                                            <div class="price">--}}
{{--                                                <span class="new-price">&#8358;{{number_format($product->price)}}</span>--}}
{{--                                                --}}{{--                                                <span class="old-price">$652.00</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="product-review">--}}
{{--                                                --}}{{--                                                <div class="rating">--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                --}}{{--                                                </div>--}}
{{--                                                --}}{{--                                                <a href="#" class="rating-count">5 reviews</a>--}}
{{--                                            </div>--}}
{{--                                            <p>{!! $product->description !!}</p>--}}
{{--                                            <div class="product-add-to-cart">--}}
{{--                                                <div class="input-counter">--}}
{{--                                        <span class="minus-btn">--}}
{{--                                            <i class='bx bx-minus'></i>--}}
{{--                                        </span>--}}
{{--                                                    <input type="text" value="1">--}}
{{--                                                    <span class="plus-btn">--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                        </span>--}}
{{--                                                </div>--}}
{{--                                                <a href="{{url('add-to-cart/'.$product->slug)}}" class="default-btn">--}}
{{--                                                    Add to cart--}}
{{--                                                </a>--}}
{{--                                            </div>--}}

{{--                                            --}}{{--                                            <div class="buy-checkbox-btn">--}}
{{--                                            --}}{{--                                                <div class="item">--}}
{{--                                            --}}{{--                                                    <input class="inp-cbx" id="cbx" type="checkbox">--}}
{{--                                            --}}{{--                                                    <label class="cbx" for="cbx">--}}
{{--                                            --}}{{--                                            <span>--}}
{{--                                            --}}{{--                                                <svg width="12px" height="10px" viewbox="0 0 12 10">--}}
{{--                                            --}}{{--                                                    <polyline points="1.5 6 4.5 9 10.5 1"></polyline>--}}
{{--                                            --}}{{--                                                </svg>--}}
{{--                                            --}}{{--                                            </span>--}}
{{--                                            --}}{{--                                                        <span>I agree with the terms and conditions</span>--}}
{{--                                            --}}{{--                                                    </label>--}}
{{--                                            --}}{{--                                                </div>--}}
{{--                                            --}}{{--                                                <div class="item">--}}
{{--                                            --}}{{--                                                    <a href="#" class="btn btn-light">Buy it now!</a>--}}
{{--                                            --}}{{--                                                </div>--}}
{{--                                            --}}{{--                                            </div>--}}

{{--                                            --}}{{--                                            <div class="products-share">--}}
{{--                                            --}}{{--                                                <ul class="social">--}}
{{--                                            --}}{{--                                                    <li><span>Share:</span></li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                    <li>--}}
{{--                                            --}}{{--                                                        <a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>--}}
{{--                                            --}}{{--                                                    </li>--}}
{{--                                            --}}{{--                                                </ul>--}}
{{--                                            --}}{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <!-- End QuickView Modal Area -->
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Most Viewed Products Area -->

    <!-- Start Testimonial Area -->
    <section class="testimonial-area ptb-100">
        <div class="container">
            <div class="section-title">
                <h2>Client’s Feedback</h2>
                <p>KlayFoods TomBrown is the new rave and here’s what some of our customers have had to say about it.
                </p>
            </div>

            <div class="testimonial-slider owl-carousel owl-theme">
                <div class="testimonial-item">
                    <div class="info">
                        <img src="{{asset('assets/img/tolani.jpeg')}}" alt="image">
                        <h3>Tola</h3>
{{--                        <span>Organic Farmer</span>--}}
                    </div>
                    <p>I absolutely love The Tom Brown breakfast meal by Klayfoods. As we all know, breakfast is the most important meal of the
                        day and I can’t think of a better way to start your day than with a nourishing and healthy meal. Its honestly quick and
                        easy to make especially if you are like me who is always rushing in the morning. It’s yummy, filling and affordable.
                        That’s a win for me ☺️
                    </p>

                    <div class="icon">
                        <i class="flaticon-right-quotes-symbol"></i>
                    </div>
                </div>

                <div class="testimonial-item">
                    <div class="info">
                        <img src="{{asset('assets/img/aminat.jpeg')}}" alt="image">
                        <h3>Aminat</h3>
                        {{--                        <span>Organic Farmer</span>--}}
                    </div>
                    <p>I eventually fell in love with NanaKlay foods after being reluctant,I’m definitely coming to get more,the first day my aunt tried it,she also loved it besides it’s filling.
                    </p>

                    <div class="icon">
                        <i class="flaticon-right-quotes-symbol"></i>
                    </div>
                </div>

                <div class="testimonial-item">
                    <div class="info">
                        <img src="{{asset('assets/img/emordi.jpg')}}" alt="image">
                        <h3>Emordi</h3>
                        {{--                        <span>Organic Farmer</span>--}}
                    </div>
                    <p>
                        I got Klayfoods(tombrown) and it has been top notch. I have had it for breakfast on several  occasions and it has lived up to it's expectation
                    </p>

                    <div class="icon">
                        <i class="flaticon-right-quotes-symbol"></i>
                    </div>
                </div>



            </div>
        </div>
    </section>
    <!-- End Testimonial Area -->

    <!-- Start Blog Area -->
    <section class="blog-area pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <h2>Latest Blog</h2>
                <p>Ready for some light reading? Head over to our blog and see what wellness and cooking tips we have for you today.</p>
            </div>

            <div class="row">
                @foreach($blogs as $blg)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog-item">
                        <div class="image">
                            <a href="{{url('blog/'.$blg->slug)}}"><img src="{{$blg->image}}" alt="image"></a>

                            <div class="date">
                                <span>{{\Carbon\Carbon::parse($blg->created_at)->diffForHumans()}}</span>
                            </div>
                        </div>
                        <div class="content">
                            <h3>
                                <a href="{{url('blog/'.$blg->slug)}}">{{$blg->title}}</a>
                            </h3>
                            <a href="{{url('blog/'.$blg->slug)}}" class="blog-btn">Read More +</a>
                        </div>
                    </div>
                </div>
                @endforeach

{{--                <div class="col-lg-4 col-md-6">--}}
{{--                    <div class="single-blog-item">--}}
{{--                        <div class="image">--}}
{{--                            <a href="blog-details.html"><img src="assets/img/blog/blog-2.jpg" alt="image"></a>--}}

{{--                            <div class="date">--}}
{{--                                <span>24 December 2020</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="content">--}}
{{--                            <h3>--}}
{{--                                <a href="blog-details.html">Better Hot Drink Elegant You Can Order By Online</a>--}}
{{--                            </h3>--}}
{{--                            <a href="blog-details.html" class="blog-btn">Read More +</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">--}}
{{--                    <div class="single-blog-item">--}}
{{--                        <div class="image">--}}
{{--                            <a href="blog-details.html"><img src="assets/img/blog/blog-3.jpg" alt="image"></a>--}}

{{--                            <div class="date">--}}
{{--                                <span>29 December 2020</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="content">--}}
{{--                            <h3>--}}
{{--                                <a href="blog-details.html">Role of Genetics in Treating Low-grade Glioma</a>--}}
{{--                            </h3>--}}
{{--                            <a href="blog-details.html" class="blog-btn">Read More +</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Blog Area -->
    <!-- Start Newsletter Area -->
    <div class="newsletter-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="newsletter-content">
                        <h2>Subscribe To Our Newsletter</h2>
                        <p>Never miss a thing. Subscribe to our newsletter and get quick first hand information on our latest releases and best deals.</p>
                    </div>
                </div>

                <div class="col-lg-6">
                    @if(session('newsletter'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('newsletter') }}
                        </div>
                    @endif

                        @if(session('newsletter_error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{ session('newsletter_error') }}
                            </div>
                        @endif
                    <form class="newsletter-forms" action="{{url('newsletter')}}" method="POST">
                        @csrf
                        <input type="email" class="input-newsletter" placeholder="Enter Email Address" name="email" required autocomplete="on">

                        <button type="submit">Subscribe Now</button>

{{--                        <div id="validator-newsletter" class="form-result"></div>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Newsletter Area -->


@endsection
