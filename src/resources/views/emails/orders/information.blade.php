@component('mail::message')
# New Order!!

A new order has been made.<br>

Buyer Information<br>
<hr>
Buyer Name: {{$buyer}}<br>
Buyer Email: {{$buyer_email}}<br>
Buyer Phone: {{$buyer_phone}}<br>
{{--Buyer Address: {{$buyer_address}}<br>--}}
{{--Buyer City: {{$buyer_city}}<br>--}}
{{--Buyer State: {{$buyer_state}}<br>--}}
<hr>
Order Info<br>
Product Amount : &#8358; {{number_format($amount,2)}}<br>
Transaction Reference : {{$reference_number}}<br>
<br><br>
<p><i>Please Use the <b>Transaction Reference</b> to search for Customer Order!</i></p>
<p><i>When logged in as an admin, Click on Transactions on the side bar, <br>
        Paste the transaction reference on search, <br>
        Then click the Transaction Id to view Items bought and total
    </i></p>
{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
Klayfoods.com
{{--{{ config('app.name') }}--}}
@endcomponent
