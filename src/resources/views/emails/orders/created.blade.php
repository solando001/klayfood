@component('mail::message')
# ORDER INFORMATION

Dear {{$buyer}},

Thanks for your Patronage, <br>Below is the breakdown of your order from Klayfoods

{{--Product name : {{$product}} <br>--}}
Product Amount : <b>&#8358; {{number_format($amount,2)}}</b><br>
Transaction Reference : <b>{{$reference_number}}</b><br>

<hr>

<p>Please make a payment of <b>&#8358; {{number_format($amount,2)}}</b> to the account number below<br>
    Account Name: <b>Klays Fitness and Wellness</b> <br>
    Account Number: <b>1016425348</b><br>
    Bank Name: <b>Zenith Bank</b><br>
</p>
<p>Please Send a proof of payment to klayfoods@gmail.com with the <b>Order Reference</b> and <b>Total amount</b> paid!</p>
<p><b><i>Note: Orders will not be shipped until payment is confirmed!</i></b></p>
{{--

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
KlayFoods.com
{{--{{ config('app.name') }}--}}
@endcomponent
