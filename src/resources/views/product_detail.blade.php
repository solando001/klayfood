@extends('layouts.master')
@section('title', $detail->name.' | KlayFood')

@section('content')
    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg4">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Product Details</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>{{$detail->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Product Details Area -->
    <section class="product-details-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="product-details-image"><img src="{{asset($detail->image->url)}}"></div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="product-details-desc">
                                <h3>{{$detail->name}}</h3>
                                <div class="price">
                                    <span class="new-price">&#8358;{{number_format($detail->price,2)}}</span>
{{--                                    <span class="old-price">$652.00</span>--}}
                                </div>
{{--                                <div class="product-review">--}}
{{--                                    <div class="rating">--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                    </div>--}}
{{--                                    <a href="#" class="rating-count">3 reviews</a>--}}
{{--                                </div>--}}
                                <p>{!! $detail->description !!}</p>
                                <hr>
                                <div class="product-add-to-cart">

                                    <div class="input-counter">
{{--                                            <span class="minus-btn">--}}
{{--                                                <i class='bx bx-minus'></i>--}}
{{--                                            </span>--}}
{{--                                        <input  type="text" min="1" value="1">--}}
                                        <select  name="count">
                                            <?php
                                            for($i=1; $i <= $detail->stock; $i++){
                                            ?>
                                        <option value="{{$i}}">{{$i}}</option>
                                            <?php } ?>
                                        </select>
{{--                                        <span class="plus-btn">--}}
{{--                                                <i class='bx bx-plus'></i>--}}
{{--                                            </span>--}}
                                    </div>
                                    <a href="{{url('add-to-cart/'.$detail->slug)}}" class="default-btn">
                                        Add to cart
                                        <span></span>
                                    </a>

                                    <a href="{{url('cart')}}" class="default-btn">
                                        View Cart
                                        <span></span>
                                    </a>
                                </div>

{{--                                <div class="buy-checkbox-btn">--}}
{{--                                    <div class="item">--}}
{{--                                        <input class="inp-cbx" id="cbx" type="checkbox">--}}
{{--                                        <label class="cbx" for="cbx">--}}
{{--                                                <span>--}}
{{--                                                    <svg width="12px" height="10px" viewbox="0 0 12 10">--}}
{{--                                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>--}}
{{--                                                    </svg>--}}
{{--                                                </span>--}}
{{--                                            <span>I agree with the terms and conditions</span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                    <div class="item">--}}
{{--                                        <a href="#" class="btn btn-light">Buy it now!</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="products-share">--}}
{{--                                    <ul class="social">--}}
{{--                                        <li><span>Share:</span></li>--}}
{{--                                        <li>--}}
{{--                                            <a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>

                    <div class="tab products-details-tab">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <ul class="tabs">
                                    <li>
                                        <a href="#">
                                            <div class="dot"></div>
                                            Description
                                        </a>
                                    </li>
{{--                                    <li>--}}
{{--                                        <a href="#">--}}
{{--                                            <div class="dot"></div>--}}
{{--                                            Reviews--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="tab_content">
                                    <div class="tabs_item">
                                        <div class="products-details-tab-content">
                                            <p>{!! $detail->description !!}</p>
                                        </div>
                                    </div>

                                    <div class="tabs_item">
                                        <div class="products-details-tab-content">
                                            <div class="product-review-form">
                                                <h3>Customer Reviews</h3>
                                                <div class="review-title">
                                                    <div class="rating">
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                    </div>
                                                    <p>Based on 3 reviews</p>
                                                    <a href="#" class="default-btn">
                                                        Write a Review
                                                    </a>
                                                </div>
                                                <div class="review-comments">
                                                    <div class="review-item">
                                                        <div class="rating">
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                        </div>
                                                        <h3>Good</h3>
                                                        <span><strong>Admin</strong> on <strong>Sep 21, 2020</strong></span>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                                        <a href="#" class="review-report-link">Report as Inappropriate</a>
                                                    </div>
                                                    <div class="review-item">
                                                        <div class="rating">
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                        </div>
                                                        <h3>Good</h3>
                                                        <span><strong>Admin</strong> on <strong>Sep 21, 2020</strong></span>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                                        <a href="#" class="review-report-link">Report as Inappropriate</a>
                                                    </div>
                                                    <div class="review-item">
                                                        <div class="rating">
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                        </div>
                                                        <h3>Good</h3>
                                                        <span><strong>Admin</strong> on <strong>Sep 21, 2020</strong></span>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                                        <a href="#" class="review-report-link">Report as Inappropriate</a>
                                                    </div>
                                                </div>
                                                <div class="review-form">
                                                    <h3>Write a Review</h3>
                                                    <form>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" id="name" name="name" placeholder="Enter your name" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <input type="email" id="email" name="email" placeholder="Enter your email" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" id="review-title" name="review-title" placeholder="Enter your review a title" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <textarea name="review-body" id="review-body" cols="30" rows="7" placeholder="Write your comments here" class="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12">
                                                                <button type="submit" class="default-btn">
                                                                    Submit Review
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

{{--                    <div class="related-shop">--}}
{{--                        <h4>Related Products</h4>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-6 col-md-6">--}}
{{--                                <div class="top-products-item">--}}
{{--                                    <div class="products-image">--}}
{{--                                        <a href="shop-details.html"><img src="assets/img/top-products/top-products-1.jpg" alt="image"></a>--}}

{{--                                        <ul class="products-action">--}}
{{--                                            <li>--}}
{{--                                                <a href="cart.html" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#productsQuickView">--}}
{{--                                                    <i class="flaticon-search"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}

{{--                                        <div class="sale">--}}
{{--                                            <span>Sale</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="products-content">--}}
{{--                                        <h3>--}}
{{--                                            <a href="shop-details.html">Darling Oranges</a>--}}
{{--                                        </h3>--}}
{{--                                        <div class="price">--}}
{{--                                            <span class="new-price">$38.00</span>--}}
{{--                                            <span class="old-price">$125.00</span>--}}
{{--                                        </div>--}}
{{--                                        <ul class="rating">--}}
{{--                                            <li>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bx-star'></i>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-lg-6 col-md-6">--}}
{{--                                <div class="top-products-item">--}}
{{--                                    <div class="products-image">--}}
{{--                                        <a href="shop-details.html"><img src="assets/img/top-products/top-products-2.jpg" alt="image"></a>--}}

{{--                                        <ul class="products-action">--}}
{{--                                            <li>--}}
{{--                                                <a href="cart.html" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#productsQuickView">--}}
{{--                                                    <i class="flaticon-search"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}

{{--                                        <div class="sale">--}}
{{--                                            <span>Sale</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="products-content">--}}
{{--                                        <h3>--}}
{{--                                            <a href="shop-details.html">Strawberry</a>--}}
{{--                                        </h3>--}}
{{--                                        <div class="price">--}}
{{--                                            <span class="new-price">$30.00</span>--}}
{{--                                            <span class="old-price">$116.00</span>--}}
{{--                                        </div>--}}
{{--                                        <ul class="rating">--}}
{{--                                            <li>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bx-star'></i>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-lg-6 col-md-6">--}}
{{--                                <div class="top-products-item">--}}
{{--                                    <div class="products-image">--}}
{{--                                        <a href="shop-details.html"><img src="assets/img/top-products/top-products-3.jpg" alt="image"></a>--}}

{{--                                        <ul class="products-action">--}}
{{--                                            <li>--}}
{{--                                                <a href="cart.html" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#productsQuickView">--}}
{{--                                                    <i class="flaticon-search"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}

{{--                                        <div class="sale">--}}
{{--                                            <span>Sale</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="products-content">--}}
{{--                                        <h3>--}}
{{--                                            <a href="shop-details.html">Cabbage</a>--}}
{{--                                        </h3>--}}
{{--                                        <div class="price">--}}
{{--                                            <span class="new-price">$40.00</span>--}}
{{--                                            <span class="old-price">$179.00</span>--}}
{{--                                        </div>--}}
{{--                                        <ul class="rating">--}}
{{--                                            <li>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bx-star'></i>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-lg-6 col-md-6">--}}
{{--                                <div class="top-products-item">--}}
{{--                                    <div class="products-image">--}}
{{--                                        <a href="shop-details.html"><img src="assets/img/top-products/top-products-4.jpg" alt="image"></a>--}}

{{--                                        <ul class="products-action">--}}
{{--                                            <li>--}}
{{--                                                <a href="cart.html" data-tooltip="tooltip" data-placement="top" title="Add to Cart"><i class="flaticon-shopping-cart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist"><i class="flaticon-heart"></i></a>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#productsQuickView">--}}
{{--                                                    <i class="flaticon-search"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}

{{--                                        <div class="sale">--}}
{{--                                            <span>Sale</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="products-content">--}}
{{--                                        <h3>--}}
{{--                                            <a href="shop-details.html">Nectarine</a>--}}
{{--                                        </h3>--}}
{{--                                        <div class="price">--}}
{{--                                            <span class="new-price">$45.00</span>--}}
{{--                                            <span class="old-price">$135.00</span>--}}
{{--                                        </div>--}}
{{--                                        <ul class="rating">--}}
{{--                                            <li>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bxs-star'></i>--}}
{{--                                                <i class='bx bx-star'></i>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>

{{--                <div class="col-lg-4 col-md-12">--}}
{{--                    <aside class="widget-area">--}}

{{--                        <section class="widget widget_popular_products">--}}
{{--                            <h3 class="widget-title">Popular Products</h3>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg1" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>$49.00</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Random Romance Novel Title Generator</a></h4>--}}
{{--                                    <div class="rating">--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg2" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>$59.00</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Writing Exercises Story Title Ideas</a></h4>--}}
{{--                                    <div class="rating">--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg3" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>$69.00</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Amaze Story Kitt Net's Book Ideas</a></h4>--}}
{{--                                    <div class="rating">--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                        <i class='bx bxs-star'></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </article>--}}
{{--                        </section>--}}

{{--                        <section class="widget widget_tag_cloud">--}}
{{--                            <h3 class="widget-title">Popular Tags</h3>--}}

{{--                            <div class="tagcloud">--}}
{{--                                <a href="#">Architecture</a>--}}
{{--                                <a href="#">Interior Design</a>--}}
{{--                                <a href="#">Designing</a>--}}
{{--                                <a href="#">Construction</a>--}}
{{--                                <a href="#">Buildings</a>--}}
{{--                                <a href="#">Industrial Factory</a>--}}
{{--                                <a href="#">Material</a>--}}
{{--                                <a href="#">Organic</a>--}}
{{--                                <a href="#">Food</a>--}}
{{--                                <a href="#">Tasty</a>--}}
{{--                            </div>--}}
{{--                        </section>--}}
{{--                    </aside>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Product Details Area -->


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enter your Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('save/customer/info')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">First Name</label>
                            <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" required>
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Last Name</label>
                            <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" required>
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Email Address</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Address</label>
                            <input name="address"  type="text" class="form-control @error('address') is-invalid @enderror" required>
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Phone Number</label>
                            <input type="text" name="phone" class="form-control  @error('phone') is-invalid @enderror" required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">City/Town</label>
                            <input type="text" name="city" class="form-control @error('city') is-invalid @enderror" required>
                            @error('city')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">State</label>
                            <input name="state" type="text" class="form-control @error('state') is-invalid @enderror" required>
                            @error('state')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>



                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                {{--                <div class="modal-footer">--}}
                {{--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                {{--                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>


@endsection
