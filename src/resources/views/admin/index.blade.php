@extends('layouts.admin.master')
@section('title', 'Admin Page - KlayFoods')

@section('content')

    <div class="content">
        <!-- Quick Overview -->
        <div class="row">
            <div class="col-6 col-lg-3">
                <a class="block block-rounded block-link-shadow text-center" href="{{url('admin/customer')}}">
                    <div class="block-content block-content-full">
                        <div class="font-size-h2 text-primary">{{$cust_count}}</div>
                    </div>
                    <div class="block-content py-2 bg-body-light">
                        <p class="font-w600 font-size-sm text-muted mb-0">
                            Customers
                        </p>
                    </div>
                </a>
            </div>
            <div class="col-6 col-lg-3">
                <a class="block block-rounded block-link-shadow text-center" href="javascript:void(0)">
                    <div class="block-content block-content-full">
                        <div class="font-size-h2 text-success">{{$product_count}}</div>
                    </div>
                    <div class="block-content py-2 bg-body-light">
                        <p class="font-w600 font-size-sm text-muted mb-0">
                            Products Uploaded
                        </p>
                    </div>
                </a>
            </div>
            <div class="col-6 col-lg-3">
                <a class="block block-rounded block-link-shadow text-center" href="javascript:void(0)">
                    <div class="block-content block-content-full">
                        <div class="font-size-h2 text-dark">0</div>
                    </div>
                    <div class="block-content py-2 bg-body-light">
                        <p class="font-w600 font-size-sm text-muted mb-0">
                            Orders
                        </p>
                    </div>
                </a>
            </div>
            <div class="col-6 col-lg-3">
                <a class="block block-rounded block-link-shadow text-center" href="javascript:void(0)">
                    <div class="block-content block-content-full">
                        <div class="font-size-h2 text-dark">&#8358;{{number_format($earning,2)}}</div>
                    </div>
                    <div class="block-content py-2 bg-body-light">
                        <p class="font-w600 font-size-sm text-muted mb-0">
                            Total Earnings
                        </p>
                    </div>
                </a>
            </div>
        </div>
        <!-- END Quick Overview -->

        <!-- Recent Orders -->
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">New Orders</h3>
                <div class="block-options">
                    <button type="button" class="btn btn-sm btn-alt-primary" data-toggle="class-toggle" data-target="#one-dashboard-search-orders" data-class="d-none">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            <div id="one-dashboard-search-orders" class="block-content border-bottom d-none">
                <!-- Search Form -->
                <form action="be_pages_dashboard.html" method="POST" onsubmit="return false;">
                    <div class="form-group push">
                        <div class="input-group">
                            <input type="text" class="form-control" id="one-ecom-orders-search" name="one-ecom-orders-search" placeholder="Search recent orders..">
                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-search"></i>
                                            </span>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END Search Form -->
            </div>
            <div class="block-content">
                <!-- Recent Orders Table -->
                <div class="table-responsive">
                    <table class="table table-borderless table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 120px;">Order ID</th>
                            <th>Product</th>
                            <th class="d-none d-sm-table-cell">Created</th>
{{--                            <th class="d-none d-xl-table-cell">Customer</th>--}}
                            <th>Status</th>
                            <th class="d-none d-xl-table-cell text-center">Unit Price</th>
                            <th class="text-center" style="width: 120px;">Quantity</th>
                            <th class="d-none d-sm-table-cell text-right">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td class="text-center font-size-sm">
                                <a class="font-w600" href="javascript:void(0)">
                                    <strong>{{$order->pid}}</strong>
                                </a>
                            </td>
                            <td class="d-none d-xl-table-cell text-center>
                                <a class="font-w600 href="javascript:void(0)">
                                    <strong>{{$order->product->name}}</strong>
                                </a>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm font-w600 text-muted">{{\Carbon\Carbon::parse($order->created_at)->diffForHumans()}}</td>
{{--                            <td class="d-none d-xl-table-cell font-size-sm">--}}
{{--                                <a class="font-w600" href="javascript:void(0)">{{$order->user->first_name}} {{$order->user->last_name}}</a>--}}
{{--                            </td>--}}
                            <td>
                                <span class="font-size-sm font-w600 px-2 py-1 rounded  bg-danger-light text-danger">{{$order->status}}</span>
                            </td>
                            <td class="d-none d-xl-table-cell text-center font-size-sm">
                                <a class="font-w600" href="javascript:void(0)">&#8358;{{number_format($order->product->price,2)}}</a>
                            </td>
                            <td class="d-none d-sm-table-cell text-center">
                                <span class="font-size-sm font-w600 px-2 py-1 rounded bg-body-dark">{{$order->quantity}}</span>
                            </td>
                            <td class="d-none d-sm-table-cell text-right font-size-sm">
                                <strong>&#8358;{{number_format($order->amount,2)}}</strong>
                            </td>
                        </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- END Recent Orders Table -->

                <!-- Pagination -->
                <nav aria-label="Photos Search Navigation">
                    <ul class="pagination pagination-sm justify-content-end mt-2">
                        {{ $orders->links() }}
{{--                        <li class="page-item">--}}
{{--                            <a class="page-link" href="javascript:void(0)" tabindex="-1" aria-label="Previous">--}}
{{--                                Prev--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="page-item active">--}}
{{--                            <a class="page-link" href="javascript:void(0)">1</a>--}}
{{--                        </li>--}}
{{--                        <li class="page-item">--}}
{{--                            <a class="page-link" href="javascript:void(0)">2</a>--}}
{{--                        </li>--}}
{{--                        <li class="page-item">--}}
{{--                            <a class="page-link" href="javascript:void(0)">3</a>--}}
{{--                        </li>--}}
{{--                        <li class="page-item">--}}
{{--                            <a class="page-link" href="javascript:void(0)">4</a>--}}
{{--                        </li>--}}
{{--                        <li class="page-item">--}}
{{--                            <a class="page-link" href="javascript:void(0)" aria-label="Next">--}}
{{--                                Next--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                </nav>
                <!-- END Pagination -->
            </div>
        </div>
        <!-- END Recent Orders -->
    </div>

    @endsection
