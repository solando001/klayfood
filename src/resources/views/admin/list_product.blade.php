@extends('layouts.admin.master')
@section('title', 'Product List - KlayFoods')

@section('content')
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block block-rounded">
            <div class="block-header">
                <h3 class="block-title">Products <small>List of Product</small></h3>
            </div>
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">PID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Action</th>
                        <th style="width: 15%;">Added</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;
                        $comp = \App\Models\Product::where('comp', '1')->first();
                    ?>
                    @foreach($lists as $new)
                        <tr>
                            <td class="text-center font-size-sm">{{$new->pid}}</td>
                            <td class="font-w600 font-size-sm">
                                <a href="#">{{$new->name}}</a>
                                @if($comp != true)
                                    | <a href="{{url('comp/'.$new->id)}}" class="btn btn-warning btn-sm">compulsory</a>
                                @endif
                            </td>
                            <td class="font-w600 font-size-sm">
                                &#8358; {{number_format($new->price,2)}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                @if($new->comp == '0')
                                    <a href="{{url('admin/rm/'.$new->id)}}" class="btn btn-danger btn-sm">Remove</a>
                                @endif
                            </td>
                            <td>
                                <em class="text-muted font-size-sm">{{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}</em>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>

@endsection
