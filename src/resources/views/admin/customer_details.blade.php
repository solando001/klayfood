@extends('layouts.admin.master')
@section('title', 'Customer Information - KlayFoods')

@section('content')

    <!-- Page Content -->
    <div class="content">
        <!-- Quick Actions -->
{{--        <div class="row">--}}
{{--            <div class="col-6">--}}
{{--                <a class="block block-rounded block-link-shadow text-center" href="javascript:void(0)">--}}
{{--                    <div class="block-content block-content-full">--}}
{{--                        <div class="font-size-h2 text-dark">--}}
{{--                            <i class="fa fa-pencil-alt"></i>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="block-content py-2 bg-body-light">--}}
{{--                        <p class="font-w600 font-size-sm text-muted mb-0">--}}
{{--                            Edit Customer--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-6">--}}
{{--                <a class="block block-rounded block-link-shadow text-center" href="javascript:void(0)">--}}
{{--                    <div class="block-content block-content-full">--}}
{{--                        <div class="font-size-h2 text-danger">--}}
{{--                            <i class="fa fa-times"></i>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="block-content py-2 bg-body-light">--}}
{{--                        <p class="font-w600 font-size-sm text-danger mb-0">--}}
{{--                            Remove Customer--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- END Quick Actions -->

        <!-- User Info -->
        <div class="block block-rounded">
            <div class="block-content text-center">
                <div class="py-4">
                    <div class="mb-3">
                        <img class="img-avatar" src="{{asset('assets/img/avarta.jpg')}}" alt="">
                    </div>
                    <h1 class="font-size-lg mb-0">
                        {{$user->first_name}} {{$user->last_name}} <i class="fa fa-star text-warning" data-toggle="tooltip" title="Premium Customer"></i>
                    </h1>
                    <p class="font-size-sm text-muted">Customer</p>
                </div>
            </div>
            <div class="block-content bg-body-light text-center">
                <div class="row items-push text-uppercase">
                    <div class="col-6 col-md-6">
                        <div class="font-w600 text-dark mb-1">Orders</div>
                        <a class="link-fx font-size-h3 text-primary" href="javascript:void(0)">{{$user->order->count()}}</a>
                    </div>
                    <div class="col-6 col-md-6">
                        <div class="font-w600 text-dark mb-1">Orders Value</div>
                        <a class="link-fx font-size-h3 text-primary" href="javascript:void(0)">&#8358;{{number_format($user->order->sum('amount'),2)}}</a>
                    </div>
{{--                    <div class="col-6 col-md-3">--}}
{{--                        <div class="font-w600 text-dark mb-1">Cart</div>--}}
{{--                        <a class="link-fx font-size-h3 text-primary" href="javascript:void(0)">4</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-6 col-md-3">--}}
{{--                        <div class="font-w600 text-dark mb-1">Referred</div>--}}
{{--                        <a class="link-fx font-size-h3 text-primary" href="javascript:void(0)">3</a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- END User Info -->

        <!-- Addresses -->
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Address</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Billing Address -->
                        <div class="block block-rounded block-bordered">
                            <div class="block-header border-bottom">
                                <h3 class="block-title">Shipping Address</h3>
                            </div>
                            <div class="block-content">
                                <div class="font-size-h4 mb-1">{{$user->first_name}} {{$user->last_name}}</div>
                                <address class="font-size-sm">
                                    {{$user->profile->address?$user->profile->address:'Address not yet supplied'}},<br>
                                    {{$user->profile->city?$user->profile->city:'City/Town not yet supplied'}},<br>
                                    {{$user->profile->state?$user->profile->state:'State/Province not yet supplied'}}<br><br>
                                    <i class="fa fa-phone"></i> {{$user->phone}}<br>
                                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">{{$user->email}}</a>
                                </address>
                            </div>
                        </div>
                        <!-- END Billing Address -->
                    </div>

                </div>
            </div>
        </div>
        <!-- END Addresses -->

        <!-- Past Orders -->
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Past Orders ({{$user->order->count()}})</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-borderless table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 100px;">ID</th>
                            <th class="d-none d-md-table-cell text-center">Products</th>
                            <th class="d-none d-sm-table-cell text-center">Submitted</th>
                            <th>Status</th>
                            <th class="d-none d-sm-table-cell text-right">Value</th>
{{--                            <th class="text-center">Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->order as $order)
                        <tr>
                            <td class="text-center font-size-sm">
                                <a class="font-w600" href="be_pages_ecom_order.html">
                                    <strong>{{$order->pid}}</strong>
                                </a>
                            </td>
                            <td class="d-none d-md-table-cell text-center font-size-sm">
                                <a href="javascript:void(0)">{{$order->product->name}}</a>
{{--                                <a href="javascript:void(0)"> dsfdf </a>--}}
                            </td>
                            <td class="d-none d-sm-table-cell text-center font-size-sm">{{\Carbon\Carbon::parse($order->created_at)->diffForHumans()}}</td>
                            <td>
                                <span class="badge badge-success">{{$order->status}}</span>
                            </td>
                            <td class="text-right d-none d-sm-table-cell font-size-sm">
                                <strong>&#8358;{{number_format($order->amount,2)}}</strong>
                            </td>
{{--                            <td class="text-center font-size-sm">--}}
{{--                                <a class="btn btn-sm btn-alt-secondary" href="be_pages_ecom_product_edit.html" data-toggle="tooltip" title="View">--}}
{{--                                    <i class="fa fa-fw fa-eye"></i>--}}
{{--                                </a>--}}
{{--                                <a class="btn btn-sm btn-alt-danger" href="javascript:void(0)" data-toggle="tooltip" title="Delete">--}}
{{--                                    <i class="fa fa-fw fa-times text-danger"></i>--}}
{{--                                </a>--}}
{{--                            </td>--}}
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Past Orders -->

        <!-- Private Notes -->
{{--        <div class="block block-rounded">--}}
{{--            <div class="block-header block-header-default">--}}
{{--                <h3 class="block-title">Private Notes</h3>--}}
{{--            </div>--}}
{{--            <div class="block-content">--}}
{{--                <p class="alert alert-info font-size-sm">--}}
{{--                    <i class="fa fa-fw fa-info mr-1"></i> This note will not be displayed to the customer.--}}
{{--                </p>--}}
{{--                <form action="be_pages_ecom_customer.html" onsubmit="return false;">--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="one-ecom-customer-note">Note</label>--}}
{{--                        <textarea class="form-control" id="one-ecom-customer-note" name="one-ecom-customer-note" rows="4" placeholder="Maybe a special request?"></textarea>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <button type="submit" class="btn btn-alt-success">Add Note</button>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- END Private Notes -->
    </div>
    <!-- END Page Content --

@endsection
