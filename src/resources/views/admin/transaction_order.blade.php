@extends('layouts.admin.master')
@section('title', 'List of Orders - KlayFoods')

@section('content')

    <div class="content">
        <!-- Dynamic Table with Export Buttons -->

        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Customer Information</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Billing Address -->
                        <div class="block block-rounded block-bordered">
                            <div class="block-header border-bottom">
                                <h3 class="block-title">Shipping Address</h3>
                            </div>
                            <div class="block-content">
                                <div class="font-size-h4 mb-1">{{$user->first_name}} {{$user->last_name}}</div>
                                <address class="font-size-sm">
                                    {{$user->profile->address?$user->profile->address:'Address not yet supplied'}},<br>
                                    {{$user->profile->city?$user->profile->city:'City/Town not yet supplied'}},<br>
                                    {{$user->profile->state?$user->profile->state:'State/Province not yet supplied'}}<br><br>
                                    <i class="fa fa-phone"></i> {{$user->phone}}<br>
                                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">{{$user->email}}</a>
                                </address>
                            </div>
                        </div>
                        <!-- END Billing Address -->
                    </div>

                </div>
            </div>
        </div>

        <div class="block block-rounded">
            <div class="block-header">
                <h3 class="block-title">Orders <small>Transactions for <b>{{$ref}}</b></small></h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Order ID</th>
                        <th>Prd Name</th>
                        <th>Amount</th>
                        <th>Qty</th>
                        <th style="width: 15%;">Registered</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;
                    $total = 0;
                    ?>
                    @foreach($order as $new)
                        <tr>
                            <td class="text-center font-size-sm">{{$i++}}</td>
                            <td class="font-w600 font-size-sm">
                                <a href="">{{$new->pid}}</a>
                            </td>
                            <td class="font-w600 font-size-sm">
                                {{$new->product->name}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                &#8358;{{number_format($new->amount,2)}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                {{$new->quantity}}
                            </td>
                            <td>
                                <em class="text-muted font-size-sm">{{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}</em>
                            </td>
                        </tr>
                        <?php $total += $new->amount; ?>
                    @endforeach
                    </tbody>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Total Amount+Shipping</td>
                        <td><b>&#8358; {{number_format($total+2000,2)}}</b></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>


@endsection
