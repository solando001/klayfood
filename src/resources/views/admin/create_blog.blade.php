@extends('layouts.admin.master')
@section('title', 'Add Product - KlayFoods')

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Info -->
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Create Blog Post</h3>
            </div>
            <div class="block-content">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <form action="{{url('save/blog')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="one-ecom-product-name">Title</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" id="one-ecom-product-name" name="title" value="{{ old('title') }}" required>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <!-- CKEditor (js-ckeditor-inline + js-ckeditor ids are initialized in Helpers.ckeditor()) -->
                                <!-- For more info and examples you can check out http://ckeditor.com -->
                                <label>Content</label>
                                <textarea id="js-ckeditor" name="content"  required ></textarea>

                            </div>
                            <div class="form-group">
                                <!-- Select2 (.js-select2 class is initialized in Helpers.select2()) -->
                                <!-- For more info and examples you can check out https://github.com/select2/select2 -->
                                <label for="one-ecom-product-category">Category</label>
                                <select class="js-select2 form-control" id="one-ecom-product-category" name="category" style="width: 100%;" data-placeholder="Choose one.." required>
                                    <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->

                                        <option value="Health">Health</option>
                                        <option value="Living">Living</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label>Published?</label>
                                <div class="custom-control custom-switch mb-1">
                                    <input type="checkbox" class="custom-control-input" id="one-ecom-product-published" name="published" {{ old('published') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="one-ecom-product-published"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Upload Image</label>
                                <div class="custom-file">
                                    <!-- Populating custom file input label with the selected filename (data-toggle="custom-file-input" is initialized in Helpers.coreBootstrapCustomFileInput()) -->
                                    <!-- When multiple files are selected, we use the word 'Files'. You can easily change it to your own language by adding the following to the input, eg for DE: data-lang-files="Dateien" -->
                                    <input type="file" class="custom-file-input @error('image') is-invalid @enderror" data-toggle="custom-file-input" id="example-file-input-multiple-custom" name="image" multiple required>
                                    <label class="custom-file-label" for="example-file-input-multiple-custom">Choose files</label>

                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-alt-success">Post Blog</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Info -->
    </div>
    <!-- END Page Content -->

@endsection
