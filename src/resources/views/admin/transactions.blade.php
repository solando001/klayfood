@extends('layouts.admin.master')
@section('title', 'Transaction List - KlayFoods')

@section('content')
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block block-rounded">
            <div class="block-header">
                <h3 class="block-title">Transaction <small>Payment Received</small></h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>TX Reference</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Total Amount</th>
                        <th style="width: 15%;">Made</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($transactions as $new)
                        <tr>
                            <td class="text-center font-size-sm">{{$i++}}</td>
                            <td class="font-w600 font-size-sm">
                                <a href="{{url('admin/transaction/order/'.$new->reference)}}"> {{$new->reference}}</a>
                            </td>
                            <td class="font-w600 font-size-sm">
                                {{$new->user->first_name}} {{$new->user->last_name}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                {{$new->user->email}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                {{$new->user->phone}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                &#8358;{{number_format($new->amount)}}
                            </td>
                            <td>
                                <em class="text-muted font-size-sm">{{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}</em>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>

@endsection
