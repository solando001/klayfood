@extends('layouts.admin.master')
@section('title', 'Newsletter List - KlayFoods')

@section('content')
    <div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">Newsletter <small>Emails</small></h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                <thead>
                <tr>
                    <th class="text-center" style="width: 80px;">ID</th>
                    <th>Email Address</th>
                    <th style="width: 15%;">Registered</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach($newsletters as $new)
                <tr>
                    <td class="text-center font-size-sm">{{$i++}}</td>
                    <td class="font-w600 font-size-sm">
                        {{$new->email}}
                    </td>
                    <td>
                        <em class="text-muted font-size-sm">{{\Carbon\Carbon::parse($new->created_at)->diffForHumans()}}</em>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
    </div>

@endsection
