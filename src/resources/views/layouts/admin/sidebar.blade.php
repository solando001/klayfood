<nav id="sidebar" aria-label="Main Navigation">
    <!-- Side Header -->
    <div class="content-header bg-white-5">
        <!-- Logo -->
        <a class="font-w600 text-dual" href="{{url('/home')}}">
                        <span class="smini-visible">
                            <i class="fa fa-circle-notch text-primary"></i>
                        </span>
            <span class="smini-hide font-size-h5 tracking-wider">
                            Klay<span class="font-w400">Foods</span>
                        </span>
        </a>
        <!-- END Logo -->

        <!-- Extra -->
        <div>

            <!-- END Close Sidebar -->
        </div>
        <!-- END Extra -->
    </div>
    <!-- END Side Header -->

    <!-- Sidebar Scrolling -->
    <div class="js-sidebar-scroll">
        <!-- Side Navigation -->
        <div class="content-side">
            <ul class="nav-main">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/home')}}">
                        <i class="nav-main-link-icon si si-speedometer"></i>
                        <span class="nav-main-link-name">Dashboard</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/add/product')}}">
                        <i class="nav-main-link-icon si si-plus"></i>
                        <span class="nav-main-link-name">Add Product</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/list/product')}}">
                        <i class="nav-main-link-icon si si-eye"></i>
                        <span class="nav-main-link-name">Product List</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/admin/transactions')}}">
                        <i class="nav-main-link-icon si si-anchor"></i>
                        <span class="nav-main-link-name">Transactions</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/create/blog')}}">
                        <i class="nav-main-link-icon si si-plus"></i>
                        <span class="nav-main-link-name">Create Blog</span>
                    </a>
                </li>

                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/admin/customer')}}">
                        <i class="nav-main-link-icon si si-users"></i>
                        <span class="nav-main-link-name">Customer List</span>
                    </a>
                </li>

                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/admin/newsletter')}}">
                        <i class="nav-main-link-icon si si-tag"></i>
                        <span class="nav-main-link-name">Newsletter List</span>
                    </a>
                </li>

                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/admin/inquiry')}}">
                        <i class="nav-main-link-icon si si-magic-wand"></i>
                        <span class="nav-main-link-name">Complaint</span>
                    </a>
                </li>



            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- END Sidebar Scrolling -->
</nav>
