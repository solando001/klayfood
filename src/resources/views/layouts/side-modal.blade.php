<div class="sidebar-modal">
    <div class="sidebar-modal-inner">
        <div class="sidebar-about-area">
            <div class="title">
                <h2>About Us</h2>
                <p>Klay Foods is an innovative brand dedicated to improving wellness in Nigeria, Africa and the world.
                    Klay Foods is the producer of Klay Foods' TomBrown; a high protein, high fibre cereal suitable for persons of all ages even babies.
                    Klay Foods is driven by the conviction that by the year 2025 it would have made a positive mark in the world through its easy to make, affordable and highly nutritious Klay Food's TomBrown.</p>
            </div>
        </div>

{{--        <div class="sidebar-instagram-feed">--}}
{{--            <h2>Instagram</h2>--}}
{{--            <ul>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram1.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram2.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram3.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram4.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram5.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram6.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram7.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">--}}
{{--                        <img src="assets/img/instagram/instagram8.jpg" alt="image">--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}

        <div class="sidebar-contact-area">
            <div class="contact-info">
                <div class="contact-info-content">
                    <h2>
                        <a href="tel:+2347068515706">
                            +2347068515706
                        </a>
                        <span>OR</span>
                        <a href="mailto:klayfoods@gmail.com">
                            klayfoods@gmail.com
                        </a>
                    </h2>

                    <ul class="social">
                        <li>
                            <a href="#" target="_blank">
                                <i class='bx bxl-facebook'></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class='bx bxl-twitter'></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/klayfoods/" target="_blank">
                                <i class='bx bxl-instagram'></i>
                            </a>
                        </li>
{{--                        <li>--}}
{{--                            <a href="#" target="_blank">--}}
{{--                                <i class='bx bxl-linkedin'></i>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#" target="_blank">--}}
{{--                                <i class='bx bxl-pinterest-alt'></i>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#" target="_blank">--}}
{{--                                <i class='bx bxl-youtube'></i>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </div>
        </div>

        <span class="close-btn sidebar-modal-close-btn">
                    <i class="flaticon-cancel"></i>
                </span>
    </div>
</div>
