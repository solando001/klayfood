
<!-- Start Top Header Area -->
<div class="top-header-area">

    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 text-center">
                @if(session('success'))
                    <div class="alert alert-success alert-block" id="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{ session('success') }}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{ session('error') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="row align-items-center">
            <div class="col-lg-7">
                <ul class="top-header-information">
                    <li>
                        <i class='bx bxs-map'></i>
                        2a Adewole Kuku street, off Fola Osibo, Lekki phase 1
                    </li>
                    <li>
                        <i class='bx bx-envelope'></i>
                        <a href="mailto:klayfoods@gmail.com">klayfoods@gmail.com</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-5">
                <ul class="top-header-social">
                    <li>
                        <a href="#" target="_blank">
                            <i class='bx bxl-facebook'></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/klayfoods" target="_blank">
                            <i class='bx bxl-twitter'></i>
                        </a>
                    </li>

                    <li>
                        <a href="https://www.instagram.com/klayfoods/" target="_blank">
                            <i class='bx bxl-instagram'></i>
                        </a>
                    </li>

                    @if(Auth::check())
                        |
                        <li>
                            <a href="#">
                                <i class='bx bxs-user'></i>
                                {{Auth::user()->first_name}}  {{Auth::user()->last_name}}
                                @if(Auth::user()->reseller == '1')
                                    <i class='bx bxs-star'></i>
                                    @endif
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='bx bxs-log-out'></i>
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Top Header Area -->

<div class="navbar-area">
    <div class="main-responsive-nav">
        <div class="container">
            <div class="main-responsive-menu">
                <div class="logo">
                    <a href="{{url('/')}}">
                        <img src="{{asset('assets/img/logo2.png')}}" alt="image">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="main-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{asset('assets/img/logo2.png')}}" alt="image">
                </a>

                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
{{--                            <a href="{{url('/')}}" class="nav-link active">--}}
                            <a href="{{url('/')}}" class="nav-link">
                                Home
{{--                                <i class='bx bx-chevron-down'></i>--}}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('about')}}" class="nav-link">
                                About Us
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="{{url('contact')}}" class="nav-link">
                                Contact
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('blog')}}" class="nav-link">
                                Blog
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('faq')}}" class="nav-link">
                                FAQ
                            </a>
                        </li>
                        @if(Auth::check())
                            <li class="nav-item">
                                <a href="{{url('my-orders')}}" class="nav-link">
                                    My Orders
                                </a>
                            </li>

{{--                            <li class="nav-item">--}}
{{--                                <a href="" class="nav-link">--}}
{{--                                    <i class='bx bxl-linkedin'></i>--}}
{{--                                    {{Auth::user()->first_name}}  {{Auth::user()->last_name}}--}}
{{--                                </a>--}}
{{--                            </li>--}}


                            @else
                        <li class="nav-item">
                            <a href="{{url('register')}}" class="nav-link">
                                Become A Reseller
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('login')}}" class="nav-link">
                                Login
                            </a>
                        </li>
                            @endif
                    </ul>

                    <div class="others-options d-flex align-items-center">
                        <div class="option-item">
                            <div class="languages-list">
                                <select>
                                    <option value="1">Eng</option>
                                </select>
                            </div>
                        </div>


                        <div class="option-item">
                            <div class="cart-btn">
                                @if(Auth::check())
                                    @php
                                        $value = \Illuminate\Support\Facades\Cookie::get('guest_id');
                                       $cartCount = \App\Models\Cart::where('guest_id', $value)->count();
                                    @endphp
                                    <a href="{{url('cart')}}">
                                        <i class='flaticon-shopping-cart'></i>
                                        <span>{{$cartCount}}</span>
                                    </a>

                                    @else
                                    @php
                                        $value = \Illuminate\Support\Facades\Cookie::get('guest_id');
                                        $cartCount = \App\Models\Cart::where('guest_id', $value)->count();
                                    @endphp

                                    <a href="{{url('cart')}}">
                                        <i class='flaticon-shopping-cart'></i>
                                        <span>{{$cartCount}}</span>
                                    </a>
                                    @endif
                            </div>
                        </div>

{{--                        <div class="option-item">--}}
{{--                            <form class="search-box">--}}
{{--                                <input type="text" class="form-control" placeholder="Search">--}}
{{--                                <button type="submit"><i class="flaticon-search"></i></button>--}}
{{--                            </form>--}}
{{--                        </div>--}}

                        <div class="option-item">
                            <div class="burger-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="others-option-for-responsive">
        <div class="container">
            <div class="dot-menu">
                <div class="inner">
                    <div class="circle circle-one"></div>
                    <div class="circle circle-two"></div>
                    <div class="circle circle-three"></div>
                </div>
            </div>

            <div class="container">
                <div class="option-inner">
                    <div class="others-options d-flex align-items-center">

                        <div class="option-item">
                            <div class="cart-btn">
                                @if(Auth::check())
                                    @php
                                        $value = \Illuminate\Support\Facades\Cookie::get('guest_id');
                                       $cartCount = \App\Models\Cart::where('guest_id', $value)->count();
                                    @endphp
                                    <a href="{{url('cart')}}">
                                        <i class='flaticon-shopping-cart'></i>
                                        <span>{{$cartCount}}</span>
                                    </a>

                                @else
                                    @php
                                        $value = \Illuminate\Support\Facades\Cookie::get('guest_id');
                                        $cartCount = \App\Models\Cart::where('guest_id', $value)->count();
                                    @endphp

                                    <a href="{{url('cart')}}">
                                        <i class='flaticon-shopping-cart'></i>
                                        <span>{{$cartCount}}</span>
                                    </a>
                                @endif
{{--                                <a href="cart.html">--}}
{{--                                    <i class='flaticon-shopping-cart'></i>--}}
{{--                                    <span>0</span>--}}
{{--                                </a>--}}
                            </div>
                        </div>

{{--                        <div class="option-item">--}}
{{--                            <form class="search-box">--}}
{{--                                <input type="text" class="form-control" placeholder="Search">--}}
{{--                                <button type="submit"><i class="flaticon-search"></i></button>--}}
{{--                            </form>--}}
{{--                        </div>--}}

                        <div class="option-item">
                            <div class="burger-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
