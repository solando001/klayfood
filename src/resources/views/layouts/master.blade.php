<!doctype html>
<html lang="zxx">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <!-- Meanmenu CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/meanmenu.css')}}">
    <!-- Boxicons CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/boxicons.min.css')}}">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.min.css')}}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <!-- Owl Carousel Default CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.min.css')}}">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/odometer.min.css')}}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

    <title>@yield('title')</title>

    <link rel="icon" type="image/png" href="{{asset('assets/img/fav.png')}}">


    <script>
        // $("document").ready(function(){
        //     setTimeout(function(){
        //         $("#alert").remove();
        //     }, 3000 ); // 5 secs
        //
        // });

        $(document).ready(function(){
            $(".alert").slideDown(300).delay(5000).slideUp(300);
        });
    </script>

{{--    <script src="//code.jivosite.com/widget/ZyHryov1jG" async></script>--}}
    <script src="//code.jivosite.com/widget/z0c2sLjUQa" async></script>
</head>

<body>

<!-- Start Preloader Area -->
<div class="preloader">
    <div class="preloader">
        <span></span>
        <span></span>
    </div>
</div>
<!-- End Preloader Area -->

<!-- Start Navbar Area -->
@include('layouts.navbar')
<!-- End Navbar Area -->

<!-- Sidebar Modal -->
@include('layouts.side-modal')
<!-- End Sidebar Modal -->


@yield('content')

{{--<!-- Start Newsletter Area -->--}}
{{--<div class="newsletter-area ptb-100">--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-lg-6">--}}
{{--                <div class="newsletter-content">--}}
{{--                    <h2>Subscribe To Our Newsletter</h2>--}}
{{--                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua quis ipsum suspendisse</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="col-lg-6">--}}
{{--                <form class="newsletter-form">--}}
{{--                    <input type="email" class="input-newsletter" placeholder="Enter Email Address" name="EMAIL" required autocomplete="off">--}}

{{--                    <button type="submit">Subscribe Now</button>--}}

{{--                    <div id="validator-newsletter" class="form-result"></div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- End Newsletter Area -->--}}


<!-- Start Footer Area -->
@include('layouts.footer')
<!-- End Footer Area -->


<!-- Start Go Top Area -->
<div class="go-top">
    <i class='bx bx-up-arrow-alt'></i>
</div>
<!-- End Go Top Area -->

<!-- Jquery Slim JS -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Meanmenu JS -->
<script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
<!-- Nice Select JS -->
<script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
<!-- Owl Carousel JS -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!-- Magnific Popup JS -->
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Odometer JS -->
<script src="{{asset('assets/js/odometer.min.js')}}"></script>
<!-- Jquery Appear JS -->
<script src="{{asset('assets/js/jquery.appear.min.js')}}"></script>
<!-- Ajaxchimp JS -->
<script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<!-- Form Validator JS -->
<script src="{{asset('assets/js/form-validator.min.js')}}"></script>
<!-- Contact JS -->
<script src="{{asset('assets/js/contact-form-script.js')}}"></script>
<!-- Wow JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Custom JS -->
<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>
