<div class="footer-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="single-footer-widget">
                    <div class="logo">
                        <h2>
                            <a href="{{url('/')}}">KlayFoods</a>
                        </h2>
                    </div>
                    <p>Klay Foods is an innovative brand dedicated to improving wellness in Nigeria, Africa and the world.</p>
                    <ul class="social">
                        <li>
                            <a href="#" class="facebook" target="_blank">
                                <i class='bx bxl-facebook'></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/klayfoods" class="twitter" target="_blank">
                                <i class='bx bxl-twitter'></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/klayfoods/" class="instagram" target="_blank">
                                <i class='bx bxl-instagram-alt'></i>
                            </a>
                        </li>
{{--                        <li>--}}
{{--                            <a href="#" class="linkedin" target="_blank">--}}
{{--                                <i class='bx bxl-linkedin'></i>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </div>

{{--            <div class="col-lg-3 col-sm-6">--}}
{{--                <div class="single-footer-widget">--}}
{{--                    <h3>Instagram</h3>--}}

{{--                    <ul class="instagram-list">--}}
{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram1.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram2.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram3.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram4.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram5.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram6.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram7.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram8.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <div class="box">--}}
{{--                                <img src="assets/img/instagram/instagram1.jpg" alt="image">--}}
{{--                                <i class="bx bxl-instagram"></i>--}}
{{--                                <a href="#" target="_blank" class="link-btn"></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="col-lg-4 col-sm-6">
                <div class="single-footer-widget pl-5">
                    <h3>Quick Links</h3>

                    <ul class="quick-links">
                        <li>
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li>
                            <a href="{{url('about')}}">About Us</a>
                        </li>
                        <li>
                            <a href="{{url('blog')}}">Blog</a>
                        </li>
                        <li>
                            <a href="{{url('faq')}}">FAQ</a>
                        </li>
                        <li>
                            <a href="{{url('contact')}}">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Contact Us</h3>

                    <ul class="footer-contact-info">
                        <li>
                            <i class='bx bxs-phone'></i>
                            <span>Phone</span>
                            <a href="tel:+2347068515706">+2347068515706</a>
                        </li>
                        <li>
                            <i class='bx bx-envelope'></i>
                            <span>Email</span>
                            <a href="mailto:klayfoods@gmail.com">klayfoods@gmail.com</a>
                        </li>
                        <li>
                            <i class='bx bx-map'></i>
                            <span>Address</span>
                            2a Adewole Kuku street, off Fola Osibo, Lekki phase 1
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Start Copy Right Area -->
<div class="copyright-area">
    <div class="container">
        <div class="copyright-area-content">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <p>
                        <i class="far fa-copyright"></i>
                        Copyright @<?php echo date("Y") ?> KlayFoods. All Rights Reserved.
                    </p>
                </div>

                <div class="col-lg-6 col-md-6">
                    <ul>
                        <li>
                            <a href="#">Terms & Conditions</a>
                        </li>
                        <li>
                            <a href="#">Privacy Policy</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Copy Right Area -->
