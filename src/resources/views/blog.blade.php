@extends('layouts.master')
@section('title', 'KlayFoods - Blog')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Blog</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Blog Stories</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Blog Area -->
    <section class="blog-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                @foreach($blog as $blg)
                <div class="col-lg-6 col-md-6">
                    <div class="single-blog-item">
                        <div class="image">
                            <a href="{{url('blog/'.$blg->slug)}}"><img src="{{$blg->image}}" alt="image"></a>

                            <div class="date">
                                <span>{{\Carbon\Carbon::parse($blg->created_at)->diffForHumans()}}</span>
                            </div>
                        </div>
                        <div class="content">
                            <h3>
                                <a href="{{url('blog/'.$blg->slug)}}">{{$blg->title}}</a>
                            </h3>
                            <a href="{{url('blog/'.$blg->slug)}}" class="blog-btn">Read More +</a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-lg-12 col-md-12">
                    <div class="pagination-area">
                        {{$blog->links()}}
{{--                        <a href="#" class="prev page-numbers">--}}
{{--                            <i class="flaticon-left-arrow"></i>--}}
{{--                        </a>--}}
{{--                        <a href="#" class="page-numbers">1</a>--}}
{{--                        <span class="page-numbers current" aria-current="page">2</span>--}}
{{--                        <a href="#" class="page-numbers">3</a>--}}
{{--                        <a href="#" class="page-numbers">4</a>--}}
{{--                        <a href="#" class="next page-numbers">--}}
{{--                            <i class="flaticon-right-arrow"></i>--}}
{{--                        </a>--}}
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Blog Area -->


@endsection
