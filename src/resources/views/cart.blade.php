@extends('layouts.master')
@section('title', 'Cart | KlayFood')

@section('content')


    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg2">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Cart</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Your Cart</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Cart Area -->
    <section class="cart-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                        <div class="cart-table table-responsive">
                            @if(isset($cartItems) && count($cartItems) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Unit Price</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php
                                    $Grand_total = 0;
                                    @endphp
                                @foreach($cartItems as $item)
                                    <form action="{{url('update/cart')}}" method="POST">
                                        @csrf

                                        <tr>
                                    <td class="product-thumbnail">
                                        <a href="#">
                                            <img src="{{$item->product->image->url}}" alt="item">
                                        </a>
                                    </td>
                                    <td class="product-name">
                                        <a href="{{url('product/'.$item->product->slug)}}">{{$item->product->name}}</a>
                                    </td>
                                    <td class="product-price">
                                        <span class="unit-amount">&#8358;{{number_format($item->product->price,2)}}</span>
                                    </td>
                                    <td class="product-quantity">
                                        <div class="input-counter">
{{--                                                    <span class="minus-btn">--}}
{{--                                                        <i class='bx bx-minus'></i>--}}
{{--                                                    </span>--}}
                                            <input type="text" name="qty" value="{{$item->quantity}}">
{{--                                            <span class="plus-btn">--}}
{{--                                                        <i class='bx bx-plus'></i>--}}
{{--                                                    </span>--}}
                                        </div>
                                    </td>
                                    <td class="product-subtotal">
                                        <span class="subtotal-amount">&#8358;{{number_format($item->product->price * $item->quantity,2)}}</span>
                                        <button type="submit" class="remove">
                                            <i class='bx bx-edit-alt'></i>
                                        </button>

                                        <a href="{{url('remove/item/'.$item->id.'/cart')}}" class="remove">
                                            <i class='bx bx-trash'></i>
                                        </a>
                                    </td>
                                </tr>

                                <input type="hidden" name="id" value="{{$item->id}}">

                                    @php
                                       $total = $item->product->price * $item->quantity;
                                           $Grand_total += $total;
                                    @endphp
                                    </form>
                                   @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="cart-buttons">
                            <div class="row align-items-center">
                                <div class="col-lg-7 col-sm-7 col-md-7">
                                    <a href="{{url('/')}}" class="default-btn">
                                        Back to Shop
                                    </a>
                                </div>
{{--                                <div class="col-lg-5 col-sm-5 col-md-5 text-right">--}}
{{--                                    <button type="submit" class="default-btn">--}}
{{--                                        Update Cart--}}
{{--                                    </button>--}}
{{--                                </div>--}}
                            </div>
                        </div>

                        <div class="cart-totals">
                            <h3>Cart Totals</h3>
                            <ul>
                                <li>Subtotal
                                    <span>&#8358;{{number_format($Grand_total,2)}}</span>
                                </li>
                                <li>Shipping
                                    <span>&#8358;2,000.00</span>
                                </li>
                                <li>Total
                                    <span><b>&#8358;{{number_format($Grand_total+2000,2)}}</b></span>
                                </li>
                            </ul>
                            @if(Auth::check())
                                <a href="{{url('checkout')}}" class="default-btn">
                                    Checkout as Reseller
                                </a>
                            @else
                                <a href="{{url('login')}}" class="default-btn">
                                    Checkout as Reseller
                                </a>
                            @endif
                            <?php //$ref = \Ramsey\Uuid\Uuid::uuid4();  ?>
{{--                            <a href="{{url('smart-checkout/'.$ref)}}" class="default-btn">--}}
{{--                                Checkout as Customer--}}
{{--                            </a>--}}

                            <a href="#" class="default-btn" data-toggle="modal" data-target="#exampleModal">
                                Checkout as Customer
                            </a>
                            @else
                                <div class="alert alert-info">
                                    <strong>There are no Items in your Cart</strong>
                                </div>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Cart Area -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enter your Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('save/customer/info')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">First Name</label>
                            <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" required>
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Last Name</label>
                            <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" required>
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Email Address</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Address</label>
                            <input name="address"  type="text" class="form-control @error('address') is-invalid @enderror" required>
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Phone Number</label>
                            <input type="text" name="phone" class="form-control  @error('phone') is-invalid @enderror" required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">City/Town</label>
                            <input type="text" name="city" class="form-control @error('city') is-invalid @enderror" required>
                            @error('city')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">State</label>
                            <input name="state" type="text" class="form-control @error('state') is-invalid @enderror" required>
                            @error('state')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>



                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                    <button type="button" class="btn btn-primary">Save changes</button>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>


@endsection
