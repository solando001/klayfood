@extends('layouts.master')
@section('title', 'About Us - KlayFood')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>About</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>About</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start About Area -->
    <section class="about-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about-content">
                        <h3>About Us</h3>
                        <p>Klay Foods is an innovative brand dedicated to improving wellness in Nigeria, Africa and the world.
                            Klay Foods is the producer of Klay Foods' TomBrown; a high protein, high fibre cereal suitable for persons of all ages even babies.
                            Klay Foods is driven by the conviction that by the year 2025 it would have made a positive mark in the world through its easy to make, affordable and highly nutritious Klay Food's TomBrown.</p>
                        <ul class="about-list">
                            <li><i class='bx bx-check'></i> Natural Process</li>
                            <li><i class='bx bx-check'></i> Premium Quality</li>
{{--                            <li><i class='bx bx-check'></i> Harvest Everyday</li>--}}
                            <li><i class='bx bx-check'></i> 100% Organic</li>
                            <li><i class='bx bx-check'></i> Fast Delivery</li>
                        </ul>

                        <div class="about-btn">
                            <a href="#" class="default-btn">About Us</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="about-image">
                        <img src="{{asset('assets/img/abt.jpg')}}" alt="image" style="border-radius: 5%">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Area -->

    <!-- Start Fun Facts Area -->
    <section class="fun-facts-area pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="1145">00</span>
                        </h3>
                        <p>Happy Clients</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="86">00</span>
                        </h3>
                        <p>Awards Wins</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="548">00</span>
                        </h3>
                        <p>Expert Member</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-fun-fact">
                        <h3>
                            <span class="odometer" data-count="55">00</span>
                        </h3>
                        <p>Years of Serving</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Fun Facts Area -->

@endsection
