@extends('layouts.master')
@section('title', 'KlayFoods - '.$blog->title)

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2></h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>{{$blog->title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Blog Details Area -->
    <section class="blog-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="blog-details-desc">
                        <div class="article-image">
                            <img src="{{asset($blog->image)}}" alt="image">
                        </div>

                        <div class="article-content">
                            <div class="entry-meta">
                                <ul>
                                    <li>
                                        <span>Posted:</span>
                                        <a href="#">{{\Carbon\Carbon::parse($blog->created_at)->diffForHumans()}}</a>
                                    </li>
                                    <li>
                                        <span>Posted By:</span>
                                        <a href="#">{{$blog->user->first_name}} {{$blog->user->last_name}}</a>
                                    </li>
                                </ul>
                            </div>

                            <h3>{{$blog->title}}</h3>
                            <p>{!! $blog->content !!}</p>
                            <hr>
                        </div>

                        <div class="article-footer">
                            <div class="article-tags">
                                    <span>
                                        <i class='bx bxs-bookmark'></i>
                                    </span>
                                <a href="#">{{$blog->category}}</a>
{{--                                <a href="#">Organic Food</a>--}}
                            </div>

{{--                            <div class="article-share">--}}
{{--                                <ul class="social">--}}
{{--                                    <li><span>Share:</span></li>--}}
{{--                                    <li>--}}
{{--                                        <a href="#" target="_blank">--}}
{{--                                            <i class='bx bxl-facebook'></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a href="#" target="_blank">--}}
{{--                                            <i class='bx bxl-twitter'></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a href="#" target="_blank">--}}
{{--                                            <i class='bx bxl-instagram'></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
                        </div>

{{--                        <div class="post-navigation">--}}
{{--                            <div class="navigation-links">--}}
{{--                                <div class="nav-previous">--}}
{{--                                    <a href="index.html">--}}
{{--                                        <i class="flaticon-left-arrow"></i>--}}
{{--                                        Prev Post--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                                <div class="nav-next">--}}
{{--                                    <a href="index.html">--}}
{{--                                        Next Post--}}
{{--                                        <i class="flaticon-right-arrow"></i>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="comments-area">
                            <h3 class="comments-title">{{$blog->comment->count()}} Comments:</h3>

                            <ol class="comment-list">
                                <li class="comment">

                                    @foreach($blog->comment as $comment)
                                    <article class="comment-body">
                                        <footer class="comment-meta">
                                            <div class="comment-author vcard">
                                                <img src="{{asset('assets/img/avarta.jpg')}}" class="avatar" alt="image">
                                                <b class="fn">{{$comment->name}}</b>
                                            </div>
                                            <div class="comment-metadata">
                                                <a href="#">
                                                    <time>{{\Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</time>
                                                </a>
                                            </div>
                                        </footer>
                                        <div class="comment-content">
                                            <p>{{$comment->comment}}</p>
                                        </div>
{{--                                        <div class="reply">--}}
{{--                                            <a href="#" class="comment-reply-link">Reply</a>--}}
{{--                                        </div>--}}
                                    </article>
                                        @endforeach
                                </li>
                            </ol>

                            <div class="comment-respond">
                                @if(session('success_comment'))
                                    <div class="alert alert-success alert-block" id="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('success_comment') }}
                                    </div>
                                @endif


                                <h3 class="comment-reply-title">Leave a comment</h3>

                                <form class="comment-form" action="{{url('comment')}}" method="POST">
                                    @csrf
                                    <p class="comment-notes">
                                        <span id="email-notes">Your email address will not be published.</span>
                                        Required fields are marked
                                        <span class="required">*</span>
                                    </p>
                                    <p class="comment-form-author">
                                        <label>Name <span class="required">*</span></label>
                                        <input type="text" id="author" placeholder="Your Name*" name="name" required="required">
                                    </p>
                                    <p class="comment-form-email">
                                        <label>Email <span class="required">*</span></label>
                                        <input type="email" id="email" placeholder="Your Email*" name="email" required="required">
                                    </p>
                                    <p class="comment-form-comment">
                                        <label>Comment</label>
                                        <textarea name="comment" id="comment" cols="45" placeholder="Your Comment..." rows="5" maxlength="65525" required="required"></textarea>
                                    </p>

                                    <input type="hidden" name="blog_id" value="{{$blog->id}}">

                                    <p class="form-submit">
                                        <input type="submit" name="submit" id="submit" class="submit" value="Post A Comment">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

{{--                <div class="col-lg-4 col-md-12">--}}
{{--                    <aside class="widget-area">--}}
{{--                        <section class="widget widget_orgo_posts_thumb">--}}
{{--                            <h3 class="widget-title">Popular Posts</h3>--}}
{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg1" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>June 10, 2020</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">What You Should Know About Detox Water</a></h4>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg2" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>June 21, 2020</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Better Hot Drink Elegant You Can Order By Online</a></h4>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg3" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>June 30, 2020</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Role Of Genetics In Treating Low-Grade Glioma</a></h4>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg4" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>May 10, 2020</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Organic Food Is Good For Health</a></h4>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg5" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>May 21, 2020</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Three Reasons Visibility Matters In Supply Chain</a></h4>--}}
{{--                                </div>--}}
{{--                            </article>--}}

{{--                            <article class="item">--}}
{{--                                <a href="#" class="thumb">--}}
{{--                                    <span class="fullimage cover bg6" role="img"></span>--}}
{{--                                </a>--}}
{{--                                <div class="info">--}}
{{--                                    <span>May 30, 2020</span>--}}
{{--                                    <h4 class="title usmall"><a href="#">Fusce Convallis Enim Non Magna Pharetra</a></h4>--}}
{{--                                </div>--}}
{{--                            </article>--}}
{{--                        </section>--}}

{{--                        <section class="widget widget_categories">--}}
{{--                            <h3 class="widget-title">Categories</h3>--}}

{{--                            <ul>--}}
{{--                                <li><a href="#">Architecture</a></li>--}}
{{--                                <li><a href="#">Interior Design</a></li>--}}
{{--                                <li><a href="#">Feature Products</a></li>--}}
{{--                                <li><a href="#">Organic Juices</a></li>--}}
{{--                                <li><a href="#">Dried Products</a></li>--}}
{{--                                <li><a href="#">Fruits Fresh</a></li>--}}
{{--                                <li><a href="#">Fresh Meal</a></li>--}}
{{--                            </ul>--}}
{{--                        </section>--}}

{{--                    </aside>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    <!-- End Blog Details Area -->


@endsection
