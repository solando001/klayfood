@extends('layouts.master')
@section('title', 'KlayFoods - FAQ')
@section('content')
    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>FAQ</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>FAQ</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start FAQ Area -->
    <section class="faq-area ptb-100">
        <div class="container">
            <div class="section-title">
                <h2>Asked Questions</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>

            <div class="faq-accordion">
                <ul class="accordion">
                    <li class="accordion-item">
                        <a class="accordion-title active" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            Can I change my order?
                        </a>

                        <p class="accordion-content show">
                            After an order has been paid for, it cannot be changed.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            How long will it take to get my order delivered to me?
                        </a>

                        <p class="accordion-content">
                            All orders within lagos will be delivered within 48hours, while orders outside lagos will be delivered within 5 days.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            Can I pay on delivery?
                        </a>

                        <p class="accordion-content">
                            Sadly that feature is not available yet as we are currently working on a ‘payment validates order’ basis.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            Do I get a discount if I make a bulk order?
                        </a>

                        <p class="accordion-content">
                            You get up to 20% discount when you register as a reseller and buy in bulk (% orders and above). We greatly value our resellers and are working constantly to get the best deals for them.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            Which countries do you deliver to?
                        </a>

                        <p class="accordion-content">
                            We deliver everywhere within and outside Nigeria.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            What age group is advised to consume this product?
                        </a>

                        <p class="accordion-content">
                            KlayFoods TomBrown is suitable for all age groups. Babies from 6 months upwards can take the product as well as the elderly. KlayFoods TomBrown’s rich protein base and fibre content makes it a healthy meal for all.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            What is Tom brown made from?
                        </a>

                        <p class="accordion-content">
                            Klay Foods TomBrow. is made from specially selected grains of Maize, soybeans, groundnut and dates. This recipe was put together after years of research into the nutritional value of each grain component and the quantity necessary to produce a perfect healthy blend of Tom Brown. The result is a wholesome meal, rich in nutrients and suitable for both babies and adults.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            What are the health benefits of Tom brown?
                        </a>

                        <p class="accordion-content">
                            KlayFoods’ TomBrown is rich in protein and fibre. It is also very rich in vitamins and minerals, making it a healthy meal for people of all ages.
                        </p>
                    </li>


                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            What can Tom brown be consumed with?
                        </a>

                        <p class="accordion-content">
                            The versatile nature of the food makes it a handy substitute for a lot of dishes. It can be used to bake bread or cake and consumed with a nice beverage; it can also be made as a swallow and eaten with soup, or even used as a smoothie mix. The only limitation to the use of KlayFoods’ TomBrown is the user's imagination. So by all means get creative.
                        </p>
                    </li>

                    <li class="accordion-item">
                        <a class="accordion-title" href="javascript:void(0)">
                            <i class='bx bx-plus'></i>
                            Can an individual trying to lose weight consume Tom brown?
                        </a>

                        <p class="accordion-content">
                            YES!!! Infact, KlayFoods TomBrown is recommended for all who wish to subscribe to a healthy diet. Its high protein content makes it very nutritious, high fibre content makes it feeling and keeps you going all day long without getting hungry and it is low in carbs making it a perfect blend for all who pursue a fitness journey.
                        </p>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    <!-- End FAQ Area -->


@endsection
