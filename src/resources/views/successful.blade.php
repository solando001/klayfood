@extends('layouts.master')
@section('title', 'Customer Successfully Placed Order | KlayFood')
@section('content')
    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Order Successful</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Successfully Placed Order</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Services Area -->
    <section class="services-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-organic"></i>
                        </div>
                        <h3>Order Successfully Placed!!!</h3>
                        <p>Thank you for the Order, below is the order information <br>
                        -Order Reference: <b>{{$reference->transaction_reference}}</b><br>
                        -Total Amount of  Orders: <b>&#8358; {{number_format($total,2)}}</b><br>
                        -Shipping Cost: <b>&#8358; {{number_format(2000,2)}}</b><br>
                        </p>
                        <b>Please make a payment of <b>&#8358; {{number_format($total+2000,2)}}</b> to the account number below<br>
                            Account Name: <b>Klays Fitness and Wellness</b> <br>
                            Account Number: <b>1016425348</b><br>
                            Bank Name: <b>Zenith Bank</b><br>
                        </p>
                        <p>Please Send a proof of payment to klayfoods@gmail.com with the <b>Order Reference</b> and <b>Total amount</b> paid!</p>
                        <p><b><i>Note: Orders will not be shipped until payment is confirmed!</i></b></p>
{{--                        <p>We have received your order. It will be delivered to your address in the next 5 business days. Thanks for patronage.</p>--}}
{{--                        <a href="#" class="read-btn">Read More +</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services Area -->

@endsection

