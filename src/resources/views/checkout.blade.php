@extends('layouts.master')
@section('title', 'Checkout| KlayFood')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Checkout</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>Checkout</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Checkout Area -->
    <section class="checkout-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="user-actions">
{{--                        <i class='bx bx-link-external'></i>--}}
{{--                        <span>Returning customer?--}}
{{--                                <a href="{{url('login')}}">Click here to login</a>--}}
{{--                            </span>--}}
                    </div>
                </div>
            </div>

{{--            <form action="{{url('checkout/create/account')}}" method="POST">--}}

                    @if($user->profile->address == "")
                            <form method="POST" action="{{ url('update/profile') }}">
                            @csrf
                    @else
                             <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                             @csrf

                    @endif

                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="billing-details">
                            <h3 class="title">Billing Details</h3>
                            <div class="row">
{{--                                Authenticated Forms--}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>First Name <span class="required">*</span></label>
                                        <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" value="{{ $user->first_name }} ">
                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Last Name <span class="required">*</span></label>
                                        <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" value="{{ $user->last_name }}">
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <label>Address <span class="required">*</span></label>
                                        <input name="address"  type="text" class="form-control @error('address') is-invalid @enderror" value="{{ $user->profile->address }}">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <label>Phone <span class="required">*</span></label>
                                        <input type="text" name="phone" class="form-control  @error('phone') is-invalid @enderror" value="{{ $user->phone }}">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>City <span class="required">*</span></label>
                                        <input type="text" name="city" class="form-control @error('city') is-invalid @enderror" value="{{ $user->profile->city }}">
                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>State / County <span class="required">*</span></label>
                                        <input name="state" type="text" class="form-control @error('state') is-invalid @enderror" value="{{ $user->profile->state }}">
                                        @error('state')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>


                                @if($user->profile->address == "")
                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="default-btn">
                                        Update Profile
                                    </button>
                                </div>
                                    @endif


                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="order-details">
                            <h3 class="title">Your Order</h3>
                            <div class="order-table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col">Product Name</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($ItemCheckout) && count($ItemCheckout) > 0)
                                        <?php $Grand_total = 0; $Over_total = 0;  ?>
                                        @foreach($ItemCheckout as $item)
                                        <tr>
                                            <td class="product-name">
                                                <a href="{{url('product/'.$item->product->slug)}}">{{$item->product->name}}</a>
                                            </td>
                                            <td class="product-total">
                                                <span class="subtotal-amount">&#8358;{{number_format($item->product->price * $item->quantity,2)}}</span>
                                            </td>
                                        </tr>

                                        @php
                                        if(Auth::user()->reseller == '1')
                                        {

                                                $total = $item->product->price * $item->quantity;
                                                $Over_total += $total;
                                                $per = 16/100;
                                                $per_total = $Over_total * $per;
                                                $Grand_total = $Over_total - $per_total;
                                                $final_total = $Grand_total + 2000;

                                        }else{
                                                $total = $item->product->price * $item->quantity;
                                                $Grand_total += $total;
                                        }

                                        @endphp
                                        @endforeach
                                    @else
                                    <div class="alert alert-info">
                                        No item continue <a href="{{url('/')}}">To Shop</a>
                                    </div>
                                    @endif
                                    @if(Auth::user()->reseller == '1')
                                        <tr>
                                            <td class="total-price">
                                                <span> Shipping* </span>
                                            </td>
                                            <td class="product-total">
                                                <span class="subtotal-amount">&#8358;2,000.00</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="total-price">
                                                <span> Discount </span>
                                            </td>
                                            <td class="product-subtotal">
                                                <span class="subtotal-amount">16%</span>
                                            </td>
                                        </tr>

                                    @endif

                                    <tr>
                                        <td class="total-price">
                                            <span>Order Total</span>
                                        </td>
                                        <td class="product-subtotal">
                                            <span class="subtotal-amount">&#8358;{{number_format($final_total,2)}}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="payment-box">
                                <div class="payment-method">
                                    <p>
                                        <input type="radio" id="direct-bank-transfer" name="radio-group" checked>
                                        <label for="direct-bank-transfer">Online Payment</label>
                                        We receive only payment made on orders made online only. Other payment method will be available later.
                                    </p>
{{--                                    <p>--}}
{{--                                        <input type="radio" id="paypal" name="radio-group">--}}
{{--                                        <label for="paypal">PayPal</label>--}}
{{--                                    </p>--}}
{{--                                    <p>--}}
{{--                                        <input type="radio" id="cash-on-delivery" name="radio-group">--}}
{{--                                        <label for="cash-on-delivery">Cash on Delivery</label>--}}
{{--                                    </p>--}}
                                </div>


                                    <input type="hidden" name="total" value="{{$Grand_total}}">


                                        <div class="row" style="margin-bottom:40px;">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="hidden" name="email" value="{{$user->email}}"> {{-- required --}}
                                                <input type="hidden" name="orderID" value="345">
                                                <input type="hidden" name="amount" value="{{$final_total * 100}}"> {{-- required in kobo --}}
                                                <input type="hidden" name="quantity" value="1">
                                                <input type="hidden" name="currency" value="NGN">
                                                <input type="hidden" name="metadata" value="{{ json_encode($array = ['user_id' => Auth::user()->id,]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                                                <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                                                {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}

{{--                                                <p>--}}
{{--                                                    <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">--}}
{{--                                                        <i class="fa fa-plus-circle fa-lg"></i> Pay Now!--}}
{{--                                                    </button>--}}
{{--                                                </p>--}}
                                            </div>
                                        </div>
                                    @if($user->profile->address == "")
                                        <p class="text-danger">Please Update you profile to place order*</p>
                                    @else
                                    <button type="submit" class="default-btn">
                                        Place Order
                                    </button>
                                    @endif
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

        </div>
    </section>
    <!-- End Checkout Area -->


@endsection
