@extends('layouts.master')
@section('title', 'My Order | KlayFood')


@section('content')


    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg2">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>My Orders</h2>
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Home</a>
                            </li>
                            <li>My Orders</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Cart Area -->
    <section class="cart-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <form>
                        <div class="cart-table table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Unit Price</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#">
                                            <img src="{{$order->product->image->url}}" alt="item">
                                        </a>
                                    </td>
                                    <td class="product-name">
                                        <a href="{{url('product/'.$order->product->slug)}}">{{$order->product->name}}</a>
                                    </td>
                                    <td class="product-price">
                                        <span class="unit-amount">&#8358;{{number_format($order->product->price,2)}}</span>
                                    </td>
                                    <td class="product-quantity">
                                        <div class="input-counter">
                                                   {{$order->quantity}}
                                        </div>
                                    </td>
                                    <td class="product-subtotal">
                                        <span class="subtotal-amount">&#8358;{{number_format($order->amount,2)}}</span>
                                    </td>
                                </tr>
                               @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="cart-buttons">
                            <div class="row align-items-center">
                                <div class="col-lg-7 col-sm-7 col-md-7">
                                    <a href="{{url('/')}}" class="default-btn">
                                        Back to Shop
                                    </a>
                                </div>
{{--                                <div class="col-lg-5 col-sm-5 col-md-5 text-right">--}}
{{--                                    <a href="#" class="default-btn">--}}
{{--                                        Update Cart--}}
{{--                                    </a>--}}
{{--                                </div>--}}
                            </div>
                        </div>

                        <div class="cart-totals">
                            <h3>Totals Orders</h3>
                            <ul>
                                <li>Total
                                    <span><b>&#8358;{{number_format($order_total,2)}}</b></span>
                                </li>
                            </ul>
{{--                            <a href="#" class="default-btn">--}}
{{--                                Proceed to Checkout--}}
{{--                            </a>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Cart Area -->


@endsection
