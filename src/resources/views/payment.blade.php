@extends('layouts.master')
@section('title', 'Customer Payment | KlayFood')

@section('content')

    <!-- Start Page Banner -->
    <div class="page-banner-area item-bg2">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-banner-content">
                        <h2>Cart</h2>
                        <ul>
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>Cart</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Cart Area -->
    <section class="cart-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <form method="POST" action="{{ url('complete/order') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
{{--                    <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">--}}
                        @csrf

                        <div class="cart-table table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Unit Price</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $Grand_total = 0; $Over_total = 0;  ?>
                                @foreach($Items as $item)
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#">
                                            <img src="{{$item->product->image->url}}" alt="item">
                                        </a>
                                    </td>
                                    <td class="product-name">
                                        <a href="{{url('product/'.$item->product->slug)}}">{{$item->product->name}}</a>
                                    </td>
                                    <td class="product-price">
                                        <span class="unit-amount">&#8358;{{number_format($item->product->price,2)}}</span>
                                    </td>
                                    <td class="product-quantity">
                                        {{$item->quantity}}
                                    </td>
                                    <td class="product-subtotal">
                                        <span class="subtotal-amount">&#8358;{{number_format($item->product->price * $item->quantity,2)}}</span>
                                    </td>
                                </tr>
                                @php
                                    $total = $item->product->price * $item->quantity;
                                        $Grand_total += $total;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="cart-buttons">
                            <div class="row align-items-center">
                                <div class="col-lg-7 col-sm-7 col-md-7">
                                    <a href="{{url('/')}}" class="default-btn">
                                        Back to Shop
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="cart-totals">
                            <h3>Cart Totals</h3>
                            <ul>
                                <li>Subtotal
                                    <span>&#8358;{{number_format($Grand_total,2)}}</span>
                                </li>
                                <li>Shipping
                                    <span>&#8358;2,000.00</span>
                                </li>
                                <?php $final = $Grand_total+2000 ?>
                                <li>Total
                                    <span><b>&#8358;{{number_format($final,2)}}</b></span>
                                </li>
                            </ul>


                            <input type="hidden" name="total" value="{{$final}}">
                            <input type="hidden" name="customer_id" value="{{$user->id}}">


                            <div class="row" style="margin-bottom:40px;">
                                <div class="col-md-8 col-md-offset-2">
                                    <input type="hidden" name="email" value="{{$user->email}}"> {{-- required --}}
                                    <input type="hidden" name="orderID" value="345">
                                    <input type="hidden" name="amount" value="{{$final * 100}}"> {{-- required in kobo --}}
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="currency" value="NGN">
                                    <input type="hidden" name="metadata" value="{{ json_encode($array = ['user_id' => $user->id,]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                                    <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                                    {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}

                                    {{--                                                <p>--}}
                                    {{--                                                    <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">--}}
                                    {{--                                                        <i class="fa fa-plus-circle fa-lg"></i> Pay Now!--}}
                                    {{--                                                    </button>--}}
                                    {{--                                                </p>--}}
                                </div>
                            </div>
                            <button href="#" class="default-btn" type="submit">
                                 Checkout
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Cart Area -->


@endsection
