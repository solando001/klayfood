<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('index');
//});

Route::get('/', [App\Http\Controllers\GeneralController::class, 'index']);
Route::get('about', [App\Http\Controllers\GeneralController::class, 'about']);
Route::get('contact', [App\Http\Controllers\GeneralController::class, 'contact']);
Route::get('product/{slug}', [App\Http\Controllers\GeneralController::class, 'product_details']);
Route::get('add-to-cart/{slug}', [App\Http\Controllers\GeneralController::class, 'add_to_cart']);
Route::get('cart', [App\Http\Controllers\GeneralController::class, 'cart']);
Route::get('remove/item/{id}/cart', [App\Http\Controllers\GeneralController::class, 'remove_cart']);
Route::post('update/cart', [App\Http\Controllers\GeneralController::class, 'update_cart']);
Route::post('newsletter', [App\Http\Controllers\GeneralController::class, 'newsletter']);
Route::get('blog', [App\Http\Controllers\GeneralController::class, 'blog']);
Route::get('blog/{slug}', [App\Http\Controllers\GeneralController::class, 'blog_story']);
Route::get('faq', [App\Http\Controllers\GeneralController::class, 'faq']);
Route::post('contact/request', [App\Http\Controllers\GeneralController::class, 'contact_request']);
Route::post('comment', [App\Http\Controllers\GeneralController::class, 'comment']);
Route::get('reseller/{ref}', [App\Http\Controllers\GeneralController::class, 'reseller']);
Route::get('smart-checkout/{ref}', [App\Http\Controllers\GeneralController::class, 'smart_checkout']);
Route::post('save/customer/info', [App\Http\Controllers\GeneralController::class, 'save_customer_info']);
Route::get('customer', [App\Http\Controllers\GeneralController::class, 'customer']);
Route::post('/pay', [App\Http\Controllers\PaymentController::class, 'redirectToGateway'])->name('pay');
Route::get('/payment/callback', [App\Http\Controllers\PaymentController::class, 'handleGatewayCallback']);
Route::get('customer-orders/{reference}', [App\Http\Controllers\GeneralController::class, 'customer_order']);
Route::post('complete/order', [App\Http\Controllers\GeneralController::class, 'complete_order']);

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('my-orders', [App\Http\Controllers\GeneralController::class, 'my_orders']);
    Route::get('checkout', [App\Http\Controllers\GeneralController::class, 'checkout']);
    Route::post('checkout/create/account', [App\Http\Controllers\GeneralController::class, 'checkout_create']);
//    Route::post('/pay', [App\Http\Controllers\PaymentController::class, 'redirectToGateway'])->name('pay');
//    Route::get('/payment/callback', [App\Http\Controllers\PaymentController::class, 'handleGatewayCallback']);
    Route::post('update/profile', [App\Http\Controllers\GeneralController::class, 'update_profile']);

    ///Admin Routes
    Route::get('add/product', [App\Http\Controllers\AdminController::class, 'add_product']);
    Route::post('save/product', [App\Http\Controllers\AdminController::class, 'save_product']);
    Route::get('create/blog', [App\Http\Controllers\AdminController::class, 'create_blog']);
    Route::post('save/blog', [App\Http\Controllers\AdminController::class, 'save_blog']);
    Route::get('admin/newsletter', [App\Http\Controllers\AdminController::class, 'newsletter']);
    Route::get('admin/customer', [App\Http\Controllers\AdminController::class, 'customer']);
    Route::get('admin/customer/{id}', [App\Http\Controllers\AdminController::class, 'customer_info']);
    Route::get('admin/inquiry', [App\Http\Controllers\AdminController::class, 'inquiry']);
    Route::get('admin/transactions', [App\Http\Controllers\AdminController::class, 'transactions']);
    Route::get('admin/transaction/order/{ref}', [\App\Http\Controllers\AdminController::class, 'transaction_order']);
    Route::get('list/product', [App\Http\Controllers\AdminController::class, 'list_product']);
    Route::get('admin/rm/{id}', [App\Http\Controllers\AdminController::class, 'remove_product']);
    Route::get('comp/{id}', [App\Http\Controllers\AdminController::class, 'comp_product']);
});


