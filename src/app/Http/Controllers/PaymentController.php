<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Profile;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Paystack;

class PaymentController extends Controller
{
    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
//
//        try{
//            return Paystack::getAuthorizationUrl()->redirectNow();
//        }catch(\Exception $e) {
//            return Redirect::back()->withMessage(['msg'=>'The paystack token has expired. Please refresh the page and try again.', 'type'=>'error']);
//        }
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        $this->logUserPayment($paymentDetails);

        $ref = $paymentDetails['data']['reference'];

        $value = Cookie::get('guest_id');
        if($value == null)
        {
            return redirect('cart');
        }

        ///After payment is successful
        //$getCartCookie = Cart::where('guest_id', $value)->get();
        $getCartCookie = Cart::where('guest_id', $value)->get();
        foreach ($getCartCookie as $cookie)
        {
            $unitPrice = $cookie->product->price;
            $amount = $cookie->quantity * $unitPrice;

            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $code = "";
            for ($i = 0; $i < 5; $i++) {
                $code .= $chars[mt_rand(0, strlen($chars) - 1)];
            }

            Order::create([
                'pid' => $code,
                'user_id' => $paymentDetails['data']['metadata']['user_id'],
                'product_id' => $cookie->product_id,
                'quantity' => $cookie->quantity,
                'amount' => $amount,
                'transaction_reference' => $ref
            ]);
            ///$cookie->update(['user_id', Auth::user()->id]);
        }

        Cart::where('guest_id', $value)->delete();

        if(Auth::check()){
            return redirect('my-orders');
        }else{
            return redirect('customer-orders');
        }

    }


    public function logUserPayment($paymentDetails)
    {
        Transaction::create([
            'amount'=>$paymentDetails['data']['amount'],
            'reference'=>$paymentDetails['data']['reference'],
            'user_id'=>$paymentDetails['data']['metadata']['user_id'],
            'successful'=>true,
        ]);
    }

}
