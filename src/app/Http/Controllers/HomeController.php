<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $role = $user->role()->first()->name;

        if($role == 'user')
        {
            //return redirect(url()->previous());
            return redirect('/');
        }else{
            $orders = Order::where('status', 'new_order')->orderBy('id', 'desc')->paginate('20');
            //$date = \Carbon\Carbon::today()->toDateString();
            $earning = Order::all()->sum('amount');
            $product_count = Product::all()->count();
            $role = Role::where('name', 'user')->first();
            $customer_count = $role->users()->orderBy('created_at', 'desc')->count();
            return view('admin.index', ['orders' => $orders, 'earning' => $earning, 'cust_count' => $customer_count, 'product_count' => $product_count]);
        }

    }
}
