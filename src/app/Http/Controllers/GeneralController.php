<?php

namespace App\Http\Controllers;

use App\Mail\OrderCreated;
use App\Mail\OrderInformation;
use App\Models\Blog;
use App\Models\Cart;
use App\Models\Comment;
use App\Models\Complaint;
use App\Models\Newsletter;
use App\Models\Order;
use App\Models\Product;
use App\Models\Profile;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Transaction;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;
use function Symfony\Component\String\u;

class GeneralController extends Controller
{

    use RegistersUsers;


    protected $redirectTo = RouteServiceProvider::HOME;


    public function index()
    {
        $products = Product::orderBy('id', 'desc')->where('published', '1')->get();
        $blogs = Blog::where('published', '1')->orderBy('created_at', 'desc')->take(3)->get();

        return view('index', ['products' => $products, 'blogs' => $blogs]);
    }

    public function contact()
    {
        return view('contact');
    }

    public function about()
    {
        return view('about');
    }

    public function product_details($slug)
    {
        $detail = Product::where('slug', $slug)->first();
        if($detail == null)
        {
            abort('404');
        }
        return view('product_detail', ['detail' => $detail]);
    }

    public function add_to_cart($slug)
    {
        $product = Product::where('slug', $slug)->first();
        if($product == null)
        {
            abort('404');
        }

        if(Auth::check()){
            $value = Cookie::get('guest_id');
            if($value == null){
                $guest_id = str_replace('-', '', Uuid::uuid4());
                $cookie = cookie('guest_id', $guest_id, 600);
                Cart::create(['user_id' => Auth::user()->id, 'product_id' => $product->id, 'quantity' => 1, 'guest_id' => $guest_id]);
                //return back()->cookie($cookie);
                return back()->with(['success' => $product->name. ' added to cart!'])->cookie($cookie);
//                return $cookie;
            }else{
                $cartExist = Cart::where('guest_id', $value)->where('product_id', $product->id)->where('user_id', Auth::user()->id)->first();
                if($cartExist != null){
                    $count = $cartExist->quantity;
                    $cartExist->update(['quantity' =>$count+1]);
                    //return redirect('cart')->cookie($value);
                    //return back()->cookie($value);
                    return back()->with(['success' => $product->name. ' added to cart!'])->cookie($value);
//
                }
                Cart::create(['user_id' => Auth::user()->id, 'product_id' => $product->id, 'quantity' => 1, 'guest_id' => $value]);
                //return redirect('cart')->cookie($value);
                //return back()->cookie($value);
                return back()->with(['success' => $product->name. ' added to cart!'])->cookie($value);
//
            }

        }else{
            $value = Cookie::get('guest_id');
            if($value == null){
                $guest_id = str_replace('-', '', Uuid::uuid4());
                $cookie = cookie('guest_id', $guest_id, 600);
                Cart::create(['user_id' => null, 'product_id' => $product->id, 'quantity' => 1, 'guest_id' => $guest_id]);
                //return back()->cookie($cookie);
                //return back()->with(['success' => 'Action Successful!!'])->cookie($cookie);
                return back()->with(['success' => $product->name. ' added to cart!'])->cookie($cookie);
//
//                return $cookie;
            }else{
                $cartExist = Cart::where('guest_id', $value)->where('product_id', $product->id)->first();
                if($cartExist != null){
                    $count = $cartExist->quantity;
                    $cartExist->update(['quantity' =>$count+1]);
                    //return redirect('cart')->cookie($value);
                    //return back()->cookie($value);
                    //return back()->with(['success' => 'Action Successful!!'])->cookie($value);
                    return back()->with(['success' => $product->name. ' added to cart!'])->cookie($value);
//
                }
                Cart::create(['user_id' => null, 'product_id' => $product->id, 'quantity' => 1, 'guest_id' => $value]);
                //return redirect('cart')->cookie($value);
                //return back()->cookie($value);
                //return back()->with(['success' => 'Action Successful!!'])->cookie($value);
                return back()->with(['success' => $product->name. ' added to cart!'])->cookie($value);
//
            }
        }
    }

    public function cart()
    {
        if(Auth::check()){
            $value = Cookie::get('guest_id');
            //dd($value);
            $cartItems = Cart::where('guest_id', $value)->get();
        }else{
            $value = Cookie::get('guest_id');
            //dd($value);
            $cartItems = Cart::where('guest_id', $value)->get();
        }
        return view('cart', ['cartItems' => $cartItems]);
    }

    public function checkout()
    {
        if(Auth::check()){
            $value = Cookie::get('guest_id');
            if($value == null)
            {
                abort(404);
            }
            $ItemCheckout = Cart::where('guest_id', $value)->get();//where('user_id', Auth::user()->id)->get();
            $user = User::where('id', Auth::user()->id)->first();
            return view('checkout', ['ItemCheckout' => $ItemCheckout, 'user' => $user]);
        }else{

            $value = Cookie::get('guest_id');
            if($value == null)
            {
                abort(404);
            }
            //dd($value);
            $ItemCheckout = Cart::where('guest_id', $value)->get();
            return view('checkout', ['ItemCheckout' => $ItemCheckout]);

        }
    }



    public function remove_cart($id)
    {
        $cartId = Cart::where('id', $id)->first()->delete();
        //return back();
        return back()->with(['success' => 'Item deleted from cart!']);
//
    }

    public function update_cart(Request $request)
    {
        $updateCart = Cart::where('id', $request->id)->first();
        $updateCart->update(['quantity' => $request->qty]);
        return back()->with(['success' => 'Item updated Successfully!']);
//        return back();
    }

    public function checkout_create(Request $request)
    {

            $this->validate($request, [
                'first_name' => ['required','string','max:255'],
                'last_name' => ['required','string','max:255'],
                'address' => ['required','string','max:255'],
                'city' => ['required','string','max:255'],
                'state' => ['required','string','max:255'],
                'phone' => ['required', 'numeric', 'digits:11'],
//                'email' => ['required', 'string', 'email', 'max:255'],
                //'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            $update = User::findOrFail(Auth::user()->id);
            $update->update(['first_name' => $request->first_name, 'last_name' => $request->last_name, 'phone' => $request->phone]);

            $update_profile = Profile::where('user_id', Auth::user()->id)->first();
            $update_profile->update(['city' => $request->city, 'address' => $request->address, 'state' => $request->state]);

            $value = Cookie::get('guest_id');
            if($value == null)
            {
                return redirect('cart');
            }

            ///After payment is successful
            //$getCartCookie = Cart::where('guest_id', $value)->get();
            $getCartCookie = Cart::where('guest_id', $value)->get();
            foreach ($getCartCookie as $cookie)
            {
                $unitPrice = $cookie->product->price;
                $amount = $cookie->quantity * $unitPrice;

                $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $code = "";
                for ($i = 0; $i < 5; $i++) {
                    $code .= $chars[mt_rand(0, strlen($chars) - 1)];
                }
                $processOrder = Order::create([
                    'pid' => $code,
                    'user_id' => Auth::user()->id,
                    'product_id' => $cookie->product_id,
                    'quantity' => $cookie->quantity,
                    'amount' => $amount,
                ]);
                ///$cookie->update(['user_id', Auth::user()->id]);
            }

            $clearcart = Cart::where('guest_id', $value)->delete();

            return redirect('my-orders');

    }

    public function my_orders()
    {
        $orders = Order::where('user_id', Auth::user()->id)->get();
        $order_total = Order::where('user_id', Auth::user()->id)->sum('amount');
        return view('order_list', ['orders' => $orders, 'order_total' => $order_total]);
    }

    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'address' => ['required','string','max:255'],
            'city' => ['required','string','max:255'],
            'state' => ['required','string','max:255'],
        ]);

        $update_profile = Profile::where('user_id', Auth::user()->id)->first();
        $update_profile->update(['city' => $request->city, 'address' => $request->address, 'state' => $request->state]);
        return back()->with(['success' => 'Profile Updated Successfully, You can now place your order!']);
    }

    public function newsletter(Request $request)
    {
        $newsletter = Newsletter::where('email', $request->email)->first();
        if($newsletter == null){
            $newsletter = Newsletter::create(['email' => $request->email]);
            return back()->with(['success' => 'Thanks your email has been recorded successfully']);
        }
        return back()->with(['error' => 'Email already recorded']);
    }

    public function blog()
    {
        $blog = Blog::where('published', '1')->orderBy('created_at', 'desc')->paginate(12);
        return view('blog', ['blog' => $blog]);
    }

    public function blog_story($slug)
    {
        if($slug ==null)
        {
            abort('404');
        }
        $details = Blog::where('slug', $slug)->first();
        $count = $details->count;
        $details->update(['count' => $count+1]);
        return view('blog-detail', ['blog' => $details]);
    }

    public function faq()
    {
        return view('faq');
    }

    public function contact_request(Request $request)
    {
        $this->validate($request, [
            'name' => ['required','string','max:255'],
            'email' => ['required','email','max:255'],
            'phone' => ['required','numeric','digits:11'],
            'subject' => ['required','string','max:255'],
            'message' => ['required','string'],
        ]);

        $com = Complaint::create($request->all());
        $com->save();

        return back()->with(['success_contact' => 'Thank you very much, your '.$request->category .' has been submitted successfully']);

    }

    public function comment(Request $request)
    {
        $comment = Comment::create($request->all());
        $comment->save();

        return back()->with(['success_comment' => 'Thank you, comment Posted']);

    }

    public function reseller($ref)
    {
        $value = Cookie::get('guest_id');
        if($value == null)
        {
            abort(404);
        }
        $ItemCheckout = Cart::where('guest_id', $value)->get();
        $user = User::where('id', $ref)->first();
        return view('payment', ['Items' => $ItemCheckout, 'user' => $user]);
    }

    public function complete_order(Request $request)
    {
        //dd($request);
        $value = Cookie::get('guest_id');

        if($value == null)
        {
            return redirect('cart');
        }

        $getCartCookie = Cart::where('guest_id', $value)->get();
        //$total_amount = Cart::where('guest_id', $value)->sum('amount');

        $reference = sha1(time());
        Transaction::create([
            'amount'=>$request->total,//$paymentDetails['data']['amount'],
            'reference'=>$reference,//$paymentDetails['data']['reference'],
            'user_id'=>$request->customer_id,//$paymentDetails['data']['metadata']['user_id'],
            'successful'=>true,
        ]);

        foreach ($getCartCookie as $cookie)
        {
            $unitPrice = $cookie->product->price;
            $amount = $cookie->quantity * $unitPrice;

            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $code = "";
            for ($i = 0; $i < 5; $i++) {
                $code .= $chars[mt_rand(0, strlen($chars) - 1)];
            }

            Order::create([
                'pid' => $code,
                'user_id' => $request->customer_id, //$paymentDetails['data']['metadata']['user_id'],
                'product_id' => $cookie->product_id,
                'quantity' => $cookie->quantity,
                'amount' => $amount,
                'transaction_reference' => $reference
            ]);
        }

        Cart::where('guest_id', $value)->delete();

        $order = Order::where('transaction_reference', $reference)->first();
        $buyer = User::findOrFail($request->customer_id);
        //dd($buyer);
        $total_amount = $request->total;
        Mail::to($buyer->email)->send(new OrderCreated($order, $total_amount));
        Mail::to('klayfoods@gmail.com')->send(new OrderInformation($order, $total_amount));

        if(Auth::check()){
            return redirect('my-orders');
        }else{
            return redirect('customer-orders/'.$reference);
        }
    }

    public function smart_checkout($ref)
    {
        //dd($ref);
        $value = Cookie::get('guest_id');
        if($value == null)
        {
            abort(404);
        }
        //dd($value);
        $ItemCheckout = Cart::where('guest_id', $value)->get();
        //return view('checkout', ['ItemCheckout' => $ItemCheckout]);

        return view('smart-checkout', ['ItemCheckout' => $ItemCheckout]);
    }

    public function save_customer_info(Request $request)
    {
        $check = User::where('email', $request->email)->first();
        if($check == null){
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->phone),
            ]);

            Profile::create([
                'user_id' => $user->id,
                'state' => $request->state,
                'city' => $request->city,
                'address' => $request->address,
            ]);
            $ref = $user->id;
            return redirect('reseller/'.$ref);

        }else{
//            Profile::create([
//                'user_id' => $check->id,
//                'state' => $request->state,
//                'city' => $request->city,
//                'address' => $request->address,
//            ]);
            $ref = $check->id;
            return redirect('reseller/'.$ref);
        }


    }

    public function customer()
    {
        dd('ok');
    }

    public function customer_order($reference)
    {
        $orders = Order::where('transaction_reference', $reference)->first();
        $total = Order::where('transaction_reference', $reference)->sum('amount');
        return view('successful', ['reference' => $orders, 'total' => $total]);
    }

    public function customer_payment($id)
    {
        $value = Cookie::get('guest_id');
        if($value == null)
        {
            abort(404);
        }
        $ItemCheckout = Cart::where('guest_id', $value)->get();

        return view('payment', ['Items' => $ItemCheckout, 'user' => $id]);
    }
}
