<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Complaint;
use App\Models\Newsletter;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\CommonMark\Inline\Element\Image;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add_product()
    {
        $category = Category::all();

        return view('admin.add_product', ['category' => $category]);
    }

    public function save_product(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'image' => 'required|mimes:jpeg,png,jpg|max:2014',
        ]);
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code = "";
        for ($i = 0; $i < 5; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        $pub = 0;
        if ($request->published == 'on') {
            $pub = 1;
        }

        if ($request->hasFile('image')){
            $data = $request->all();
            $data['pid'] = $code;
            $data['user_id'] = Auth::user()->id;
            $data['published'] = $pub;
            $product = Product::create($data);
            $product->save();
            $str = Str::slug($request->name, '-');
            $slug = $str . -$product->id;
            $product->update(['slug' => $slug]);
            //Storage::

           // $extension = $request->image->extension();
            $extension = time().'.'.$request->image->extension();
            //Image::
           //$url = Image::make($request->image)->resize(650, 650)->move('img_product', $extension);
            $url = $request->image->move('img_product', $extension);

            //Image::make($image)->resize(300, 300)->save( public_path('/images/' . $filename ) );

            //$url =$request->image->storeAs('img_product',  $extension);
            ////$file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);
            //$url = Storage::url('img/'.$request->name.".".$extension);
            ProductImage::create(['product_id' => $product->id, 'url' => $url]);
            return back()->with(['success' => 'Product Created Successfully']);
        }else {
            return back()->with(['error' => 'Please Select an Image to upload']);
        }
    }

    public function create_blog()
    {
        return view('admin.create_blog');
    }

    public function save_blog(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'content' => 'required|string',
            'image' => 'required|mimes:jpeg,png,jpg|max:2014',
        ]);
        $chars = "0123456789";
        $code = "";
        for ($i = 0; $i < 5; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        $pub = 0;
        if ($request->published == 'on') {
            $pub = 1;
        }
        $str = Str::slug($request->title, '-');
        $slug = $str . -$code;
        $extension = time().'.'.$request->image->extension();
        $url = $request->image->move('blog_img', $extension);

        if ($request->hasFile('image')) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['published'] = $pub;
            $data['slug'] = $slug;
            $data['image'] = $url;
            $data['count'] = 0;
            $blog = Blog::create($data);
            $blog->save();
            return back()->with(['success' => 'Blog Posted Successfully']);
        }else{
            return back()->with(['error' => 'Please Select an Image to upload']);
        }

       // dd($request);
    }

    public function newsletter()
    {
        $newsletters = Newsletter::orderBy('created_at', 'desc')->get();
        return view('admin.newsletter', ['newsletters' => $newsletters]);
    }

    public function customer()
    {
        $role = Role::where('name', 'user')->first();
        $customers = $role->users()->orderBy('created_at', 'desc')->get();
        //$customers = User::orderby('created_at', 'desc')->get();
        return view('admin.customer', ['customers' => $customers]);
    }

    public function customer_info($id)
    {
        $user = User::findOrFail($id);
        return view('admin.customer_details', ['user' => $user]);
    }

    public function inquiry()
    {
        $coms = Complaint::orderby('created_at', 'desc')->where('replied', '0')->get();
        return view('admin.inquiry', ['coms' => $coms]);
    }

    public function transactions()
    {
        $trans = Transaction::orderBy('created_at', 'desc')->get();
        return view('admin.transactions', ['transactions' => $trans]);
    }

    public function transaction_order($ref)
    {
        $order = Order::where('transaction_reference', $ref)->get();
        $user_tran = Transaction::where('reference', $ref)->first();
        $user = User::findOrFail($user_tran->user_id);
        return view('admin.transaction_order', ['order' => $order, 'ref' => $ref, 'user' => $user]);
    }

    public function list_product()
    {
        $list = Product::orderBy('created_at', 'desc')->get();
        return view('admin.list_product', ['lists' => $list]);
    }

    public function remove_product($id)
    {
        Product::findOrFail($id)->delete();
        ProductImage::where('product_id', $id)->delete();
        return back()->with(['success' => 'Product Removed Successfully']);
    }

    public function comp_product($id)
    {
        $prd = Product::findOrFail($id);
        $prd->update(['comp' => true]);
        return back()->with(['success' => 'Product is made Compulsory']);
    }

}
