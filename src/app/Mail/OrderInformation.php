<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderInformation extends Mailable
{
    use Queueable, SerializesModels;


    protected $order;
    protected $total_amount;
//    protected $buyer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $total_amount)
    {
        $this->order = $order;
        $this->total_amount = $total_amount;
//        $this->buyer = $buyer;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.information')
            ->with([
                'buyer'                     => $this->order->user->first_name, $this->order->user->last_name,
                'buyer_email'               => $this->order->user->email,
                'buyer_phone'               => $this->order->user->phone,
//                'buyer_address'             => $this->buyer->profile->address,
//                'buyer_state'               => $this->buyer->profile->state,
//                'buyer_city'                => $this->buyer->profile->city,
                'reference_number'          => $this->order->transaction_reference,
                'amount'                    => $this->total_amount,
                //'amount'            => $this->total_amount,
            ]);;
    }
}
