<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;
    protected $total_amount;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $total_amount)
    {
        $this->order = $order;
        $this->total_amount = $total_amount;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.created')
            ->with([
                'buyer'             => $this->order->user->first_name, $this->order->user->last_name,
                'reference_number'  => $this->order->transaction_reference,
                'amount'            => $this->total_amount,
                //'amount'            => $this->total_amount,
            ]);
    }
}
