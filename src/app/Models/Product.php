<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    protected $fillable = ['user_id', 'cate_id', 'pid', 'slug', 'name', 'price', 'stock', 'published', 'description', 'comp'];

    public function image()
    {
        return $this->hasOne(ProductImage::class, 'product_id', 'id');
    }
}
