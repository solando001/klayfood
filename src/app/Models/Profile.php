<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = "profiles";

    protected $fillable = ['user_id', 'state', 'city', 'address'];

//    public function user()
//    {
//        $this->belongsTo(User::class, 'user_id');
//    }
}
