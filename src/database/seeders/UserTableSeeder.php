<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $admin_role;

    public function __construct()
    {
        $this->admin_role = $role = Role::where('name', 'admin')->first();
    }

    public function run()
    {
        $user = User::create([
            'first_name' => 'Oluwatobi',
            'last_name' => 'Solomon',
            'phone' => '08137331282',
            'email' => 'solotobby@gmail.com',
            'password' => bcrypt('solomon001'),
        ]);
        RoleUser::create(['user_id' => $user->id, 'role_id' => $this->admin_role->id]);

//        --------------------------------------------------------------------------------------------------------------

        $user = User::create([
            'first_name' => 'Klay',
            'last_name' => 'Foods',
            'phone' => '07068515706',
            'email' => 'klayfoods@gmail.com',
            'password' => bcrypt('coachklay20'),
        ]);
        RoleUser::create(['user_id' => $user->id, 'role_id' => $this->admin_role->id]);


    }
}
